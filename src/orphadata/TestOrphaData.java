package orphadata;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Set;

import javax.swing.text.html.HTMLDocument.Iterator;

import org.json.JSONArray;
import org.json.JSONObject;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import Utils.Disease;

/** Class for testing connection with OrphaDatabase and some request
 * 
 * @author Elsa and Clementine 
 *
 */
public class TestOrphaData {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		OrphaDataRequest orph = new OrphaDataRequest();
		ArrayList<Disease> diseases= orph.search("Ca?tu synd*");
		for (int i=0; i<diseases.size();i++){
			System.out.println(i+": "+diseases.get(i));
		}
		System.out.println("**********************************");
		ArrayList<Disease> diseases1= orph.search("Cerebrohepatorenal syndrome");
		for (int i=0; i<diseases1.size();i++){
			System.out.println(i+": "+diseases1.get(i));
		}
		
	}
	
	

}