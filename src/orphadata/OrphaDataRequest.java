package orphadata;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import org.codehaus.jackson.JsonNode;
import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.ViewQuery;
import org.ektorp.ViewResult;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbConnector;
import org.ektorp.impl.StdCouchDbInstance;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import Utils.Disease;

/**This class creates connection with couchDB where OrphaData informations is strored
 * 
 * @author Elsa and Clementine
 *
 */

public class OrphaDataRequest {

	int compteur=0;
	private ArrayList<String> synonyms= new ArrayList<String>();

	/** this method is used to create connection with couchDB
	 * 
	 * @param url
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 */
	public JSONObject connectCouchDB(String url) throws IOException, JSONException{
		URL my_couch = new URL(url);
		HttpURLConnection con = (HttpURLConnection) my_couch.openConnection();  

		con.setRequestMethod("GET");
		con.setRequestProperty("Content-Type", "application/json");

		BufferedReader br = new BufferedReader(
				new InputStreamReader(
						con.getInputStream()));
		StringBuilder sb = new StringBuilder();
		String line; 
		while ((line = br.readLine())!=null){
			sb.append(line);
		}

		JSONObject json = new JSONObject(sb.toString()) ;



		return json;
	}

	/** this method prepares all parameters that method setDisease required
	 * 
	 * @param disease: name of the disease you search
	 * @return list of Disease
	 */
	public ArrayList<Disease> search(String disease) throws IOException, JSONException{
		
		char[] disease_table = disease.toCharArray();
		disease_table[0]=Character.toUpperCase(disease_table[0]);
		disease = new String(disease_table);
		ArrayList<Disease> diseases= new ArrayList<Disease>();
		if (disease.indexOf('*')!=-1 || disease.indexOf('?')!=-1){

			ArrayList<String> names= getDiseaseByMatchingName(disease);
			ArrayList<String> rightNames= new ArrayList<String>();
			for (int i=0; i<names.size(); i++){
				String renameDisease= disease.replaceAll("\\*",".*");
				renameDisease= renameDisease.replaceAll("\\?",".?");
				if (names.get(i).matches(renameDisease)){
					rightNames.add(names.get(i));
				}
			}

			for (int j=0; j<rightNames.size(); j++){
				JSONObject json = this.connectCouchDB(this.getDiseasesByName(rightNames.get(j)));
				Disease dis=this.setDisease(json, rightNames.get(j));
				diseases.add(dis);
			}

		}else {

			JSONObject json = this.connectCouchDB(this.getDiseasesByName(disease));
			Disease dis=this.setDisease(json,disease);
			diseases.add(dis);
		}

		return diseases;
	}

	/** This method create a disease with connection to couchDB (JSONObject) and with name of the disease
	 * 
	 * @param json: JSONObject to question orphadatabase
	 * @param disease: name of the disease we search
	 * @return DIsease
	 */
	public Disease setDisease(JSONObject json, String disease) throws IOException, JSONException{
		int orphaNumber= getOrphaNumber(json, disease);
		if (orphaNumber==0){
			return null;
		}
		String name = getDiseaseName(orphaNumber);
		ArrayList<String> symptoms= getDiseaseClinicalSignsNoLang(orphaNumber);
		ArrayList<Disease> symptomsList= new ArrayList<Disease>();
		ArrayList<String> source= new ArrayList<String>();
		source.add("OrphaData");
		for (int i=0; i<symptoms.size(); i++){
			Disease d= new Disease(symptoms.get(i).toUpperCase(),null,null,null,null,source,0);
			symptomsList.add(d);
		}
		if (symptoms.isEmpty()){
			symptomsList=null;
		}
		Disease dis;
		if (getSynonyms()==null || getSynonyms().isEmpty()){
			dis= new Disease(name.toUpperCase(),null,symptomsList,null,null,source,0);
		}else{
			dis= new Disease(name.toUpperCase(),getSynonyms(),symptomsList,null,null,source,0);
		}
		return dis;


	}
	
	
	/** This method allows to recover orphaNumber and synonyms name with disease name 
	 * 
	 * @param json: a JSONObject
	 * @param Disease: name of the disease you search
	 * @return int: corresponding to orphaNumber
	 */
	public int getOrphaNumber(JSONObject json, String Disease) throws JSONException, IOException{
		int orphaNumber=0;
		JSONArray row = json.getJSONArray("rows"); 
		synonyms= new ArrayList<String>();
		if (row.toString().equals("[]") && compteur<1){
			compteur++;
			JSONObject json1=this.connectCouchDB(getDiseasesBySynonym(Disease));
			orphaNumber=this.getOrphaNumber(json1, Disease);
		}else if (row.toString().equals("[]") && compteur==1){
			System.out.println("this disease doesn't exist in this database");;
		}else{
			String diseaseName="";

			for (int i=0; i<row.length(); i++){
				JSONObject obj= row.getJSONObject(i);
				JSONObject value= (JSONObject) obj.get("value");
				orphaNumber=value.getInt("OrphaNumber");
				//System.out.println("orphanumber: "+orphaNumber);
				JSONObject name=(JSONObject) value.get("Name");
				diseaseName=name.getString("text");
				//System.out.println("Name: "+diseaseName);
				JSONObject synonymList=(JSONObject) value.get("SynonymList");
				System.out.println(synonymList);
				if (!synonymList.isNull("Synonym")){
					if (synonymList.getInt("count")>=2){
						JSONArray synonym=synonymList.getJSONArray("Synonym");
						for (int j=0; j<synonym.length(); j++){
							JSONObject syn= synonym.getJSONObject(j);
							synonyms.add(syn.getString("text").toUpperCase());
						}
					}else {
						JSONObject synonym= synonymList.getJSONObject("Synonym");
						synonyms.add(synonym.getString("text").toUpperCase());
					}

				}

			}
		}
		return orphaNumber;
	}
	
	/** this method allows to find disease name with only an orphaNumber
	 * 
	 * @param orphaNumber
	 * @return String: disease corresponding to this orphaNumber
	 */
	public String getDiseaseName( int orphaNumber) throws IOException, JSONException{
		String diseaseName = null;
		String viewDisease = "http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/diseases/_view/GetDiseases?key="+orphaNumber;
		JSONObject json = connectCouchDB(viewDisease);
		JSONArray row = json.getJSONArray("rows"); 
		for (int i=0; i<row.length(); i++){
			JSONObject obj= row.getJSONObject(i);
			JSONObject value= (JSONObject) obj.get("value");
			JSONObject name=(JSONObject) value.get("Name");
			diseaseName=name.getString("text");

		}
		return diseaseName;

	}
	
	/** This method allows to found all names corresponding to the name jocket request
	 * 
	 * @param Disease: name of the disease with wildcards 
	 * @return list of all possible name
	 */
	public ArrayList<String> getDiseaseByMatchingName ( String Disease) throws IOException, JSONException{
		ArrayList<String> names= new ArrayList<String>();
		Disease= Disease.trim().replaceAll(" ","%20");
		String fields[]= Disease.split("\\*|\\?");
		//System.out.println("f: "+fields[0]);
		String viewDisease = "http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/diseases/_view/GetDiseasesByName?startkey=%22"+fields[0]+"%22&endkey=%22"+fields[0]+"ufff0%22";
		System.out.println("view: "+viewDisease);
		JSONObject json= connectCouchDB(viewDisease);
		JSONArray row= json.getJSONArray("rows");
		//System.out.println("n: "+json);
		for (int i=0; i<row.length(); i++){
			JSONObject obj= row.getJSONObject(i);
			//System.out.println("name: "+obj.getString("key"));
			names.add(obj.getString("key"));

		}

		return names;
	}

	/** This method creates the url for getting disease informations (orphaNumber, synonyms...) 
	 * 
	 * @param Disease: the name of the disease you are searching 
	 * @return String: url
	 */
	public String getDiseasesByName( String Disease){
		Disease= Disease.trim().replaceAll(" ","%20");
		String viewDisease = "http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/diseases/_view/GetDiseasesByName?key=%22"+Disease+"%22";
		//System.out.println(viewDisease);
		return viewDisease;

	}

	/** This method creates the url for getting disease with his synonym
	 * 
	 * @param synonymDisease: name of a disease synonym
	 * @return String: url
	 */
	public String getDiseasesBySynonym( String synonymDisease){
		synonymDisease= synonymDisease.trim().replaceAll(" ","%20");
		String viewDisease = "http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/diseases/_view/GetDiseasesBySynonym?key=%22"+synonymDisease+"%22";

		//System.out.println(viewDisease);
		return viewDisease;
	}

	/** This method allows us to recover diseases by clinical sign
	 * 
	 * @param sign: a clinical sign
	 * @return list of diseases that have this clinical sign 
	 */
	public ArrayList<String> getDiseaseByClinicalSign(String sign) throws IOException, JSONException{
		ArrayList<String> diseases=new ArrayList<String>();
		sign=sign.trim().replaceAll(" ","%20");
		String url= "http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/clinicalsigns/_view/GetDiseaseByClinicalSign?key=%22"+sign+"%22";
		JSONObject json= this.connectCouchDB(url);
		JSONArray row = json.getJSONArray("rows"); 
		for (int i=0; i<row.length(); i++){
			JSONObject obj= row.getJSONObject(i);
			JSONObject disease= (JSONObject) obj.get("disease");
			JSONObject name=(JSONObject) disease.get("Name");
			diseases.add(name.getString("text").toUpperCase());
		}

		return diseases;
	}

	/** This method allows us to recover clinical Signs with an orphaNumber corresponding to a disease
	 * 
	 * @param orphaNumber: number corresponding to a disease
	 * @return list of clinical signs for a disease
	 */
	public ArrayList<String> getDiseaseClinicalSignsNoLang(int orphaNumber) throws IOException, JSONException{
		
		ArrayList<String> clinicalSigns= new ArrayList<String>();
		String url = "http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/clinicalsigns/_view/GetDiseaseClinicalSignsNoLang?key="+orphaNumber;
		JSONObject json= this.connectCouchDB(url);
		JSONArray row = json.getJSONArray("rows"); 
		for (int i=0; i<row.length(); i++){
			String name="";
			JSONObject obj= row.getJSONObject(i);
			JSONObject value= (JSONObject) obj.get("value");
			JSONObject cS=(JSONObject) value.get("clinicalSign");
			JSONObject n= (JSONObject) cS.get("Name");
			name=n.getString("text");
			clinicalSigns.add(name.toUpperCase());

		}
		
		return clinicalSigns;
	}

	public ArrayList<String> getSynonyms() {
		return synonyms;
	}

	public void setSynonyms(ArrayList<String> synonyms) {
		this.synonyms = synonyms;
	}

}
