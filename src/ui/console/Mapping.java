package ui.console;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;

import org.apache.lucene.queryparser.classic.ParseException;
import org.json.JSONException;
import org.json.JSONObject;

import Utils.Disease;
import Utils.Drug;
import drugbank.XMLIndex;
import drugbank.XMLSearch;
import omim.OmimOntoIndex;
import omim.OmimOntoSearch;
import orphadata.OrphaDataRequest;
import slider.SQLRequest;

/**Class for mapping all results from four databases
 * 
 * @author Elsa and Clementine
 *
 */
public class Mapping {

	private OrphaDataRequest orph ;
	private SQLRequest sql;
	private OmimOntoIndex omin;
	private XMLIndex indexXML;

	/**Constructor that establishes all connection with these four databases
	 * 
	 */
	public Mapping() throws Exception{

		System.out.println("OrphaData connection...");
		orph = new OrphaDataRequest();	
		System.out.println("Done");

		System.out.println("MySQL connection...");
		sql= new SQLRequest();
		System.out.println("Done");

		Path omimTxt= Paths.get("omim.txt");  
		Path omimCsv= Paths.get("omim_onto.csv");
		Path writeTxtFile= Paths.get("omim_index");
		Path writeCsvFile= Paths.get("omim_index_csv");
		FileTime txtLastModification = Files.getLastModifiedTime(omimTxt, LinkOption.NOFOLLOW_LINKS);
		FileTime csvLastModification = Files.getLastModifiedTime(omimCsv, LinkOption.NOFOLLOW_LINKS);
		FileTime txtDateCreation = Files.getLastModifiedTime(writeTxtFile, LinkOption.NOFOLLOW_LINKS);
		FileTime csvDateCreation = Files.getLastModifiedTime(writeCsvFile, LinkOption.NOFOLLOW_LINKS);

		if (txtLastModification.compareTo(txtDateCreation)>0){
			System.out.println("Omin txt index...");
			omin= new OmimOntoIndex();
			omin.indexOMIM("omim.txt", false);
			System.out.println("Done");
		}
		if (csvLastModification.compareTo(csvDateCreation)>0){
			System.out.println("Omin csv index...");
			omin= new OmimOntoIndex();
			omin.indexOMIM("omim_onto.csv", true);
			System.out.println("Done");
		}

		Path drugbank= Paths.get("drugbank.xml");  
		Path writeDrugbankFile= Paths.get("drugbank_index");
		FileTime drugbankLastModification = Files.getLastModifiedTime(drugbank, LinkOption.NOFOLLOW_LINKS);
		FileTime drugbankDateCreation = Files.getLastModifiedTime(writeDrugbankFile,LinkOption.NOFOLLOW_LINKS);
		if (drugbankLastModification.compareTo(drugbankDateCreation)>0){

			System.out.println("XML index...");
			indexXML= new XMLIndex("drugbank.xml");
			indexXML.index();
			System.out.println("Done");
		}

	}

	/** this method transforms an ArrayList of Disease to a Disease by uniting all the element
	 * 
	 * @param d: an arrayList of Disease element
	 * @return Disease element
	 */
	public Disease result(ArrayList<Disease> d){
		Disease dis;
		if (d!=null && d.size()>=2){
			dis=Disease.union(d.get(0), d.get(1));
			for(int i=2; i<d.size(); i++){
				dis=Disease.union(dis, d.get(i));
			}

		}else if(d!=null && !d.isEmpty()){
			dis=d.get(0);
		}else {
			dis=null;
		}
		return dis;
	}

	/**This method recovers all informations from these four databases about one request 
	 * 
	 * @param d: ArrayList Disease result from OrphaDatabase
	 * @param d1 ArrayList Disease result from Omim txt
	 * @param d2 ArrayList Disease result from Omim csv
	 * @param d3 ArrayList Disease result from Slider 2
	 * @param d4 ArrayList Disease result from DrugBank
	 * @return
	 */
	public Disease Results(ArrayList<Disease> d, ArrayList<Disease> d1, ArrayList<Disease> d2, ArrayList<Disease> d3, ArrayList<Disease> d4){
		Disease dis=this.result(d);
		Disease dis1=this.result(d1);
		Disease dis2=this.result(d2);
		Disease dis3=this.result(d3);
		Disease dis4=this.result(d4);
		dis=Disease.union(dis, dis1);
		dis=Disease.union(dis, dis2);
		dis=Disease.union(dis, dis3);
		dis=Disease.union(dis, dis4);
		return dis;
	}

	/** This method is the method mapping for symptoms
	 * 
	 * @param symptom: a string that is your request
	 * @return  it's all diseases that have this symptom
	 */
	public ArrayList<String> mapSymptoms(String symptom) throws IOException, JSONException, ParseException{
		//omim
		OmimOntoSearch search= new OmimOntoSearch();

		ArrayList<String> d=search.searchSymptoms("omim_index",symptom,"Symptoms");

		//Json
		ArrayList<String> d1=orph.getDiseaseByClinicalSign(symptom);

		ArrayList<String> results=new ArrayList<String>();
		if (d==null || d.isEmpty()){
			if(d1==null || d1.isEmpty()){
				results=null;
			}
			else{
				results=d1;
			}
		}else if (d1==null || d1.isEmpty()){
			results=d;
		}else {
			for (int i=0; i<d1.size(); i++){
				if (!d.contains(d1.get(i))){
					d.add(d1.get(i));
				}
			}
			results=d;
		}


		return results;
	}

	/** This method is use to print Symptom results
	 * 
	 * @param symptom
	 * @param results
	 */
	public static void printSymptom(String symptom, ArrayList<String> results){
		System.out.println("==================Results================");
		System.out.println("");
		System.out.println("symptoms: "+symptom);
		System.out.println("");
		System.out.println("******************DISEASES************");
		System.out.println("");
		if (results!=null){
			for (int i=0; i<results.size();i++){
				System.out.println(i+": name: "+results.get(i));
			}
		}else{
			System.out.println("no diseases");
		}
		System.out.println("");
	}

	/** This method is the method mapping for drug
	 * 
	 * @param drug: a string that is your request
	 * @return Drug: all informations about drug you request
	 */
	public Drug mapDrug(String drug) throws SQLException, ParseException{
		//mysql
		Drug d1=sql.SQLRequest2(drug);

		//xml
		XMLSearch searchXML= new XMLSearch();
		Drug d2= searchXML.drug(drug);
		if (d2!=null && d1==null){
			d1=sql.SQLRequest2(d2.getName());
		}else if (d1!=null && d2==null){
			d2=searchXML.drug(d1.getName());
		}
		Drug d;
		if(d1!=null){
			d=Drug.union(d1,d2);
		}else {
			if (d2!=null){
				d=d2;
			}else{
				d= new Drug(drug,null,null,null,1);
			}
		}

		
		return d; 
	}
	
	/** This method is used to print Drug results
	 * 
	 * @param d
	 */
	public static void printDrug(Drug d){
		System.out.println("==================Results================");
		System.out.println("");
		System.out.println("name: "+d.getName());
		System.out.println("");
		System.out.println("******************SOURCES************");
		System.out.println("");
		System.out.println("sources: "+d.getSources());
		System.out.println("");
		System.out.println("******************INDICATIONS************");
		System.out.println("");
		if (d.getIndications()==null){
			System.out.println("no indications");
		} else {
			Collections.sort(d.getIndications());
			for (int i=0; i<d.getIndications().size();i++){
				if (d.getIndications().get(i)!=null && !d.getIndications().get(i).trim().equals("")){
					System.out.println(i+": "+d.getIndications().get(i));
				}
			}
		}
		System.out.println("");
		System.out.println("******************SIDE EFFECTS***********");
		System.out.println("");
		if (d.getSide_effect()==null){
			System.out.println("no side effects");
		} else {
			Collections.sort(d.getSide_effect());
			for (int i=0; i<d.getSide_effect().size();i++){
				if (d.getSide_effect().get(i)!=null && !d.getSide_effect().get(i).trim().equals("")){
					System.out.println(i+": "+d.getSide_effect().get(i));
				}
			}
		}
		System.out.println("");
	}

	/** This method is the method mapping for disease
	 * 
	 * @param disease: a string that is your request
	 * @return Disease: all informations about disease you request
	 */
	public Disease mapDisease(String disease) throws Exception{
		disease=disease.toLowerCase();

		//orphadata

		ArrayList<Disease> d= orph.search(disease);

		//omim 

		OmimOntoSearch search= new OmimOntoSearch();
		ArrayList<Disease> d1=search.Search("omim_index","Name",disease);
		if (d1==null){
			d1=search.Search("omim_index","Synonyms",disease);
			if (d1!=null && d==null){
				d=orph.search(d1.get(1).getName());
			}
		}
		ArrayList<Disease> d2=search.Search("omim_index_csv","diseaseName",disease);
		if (d2==null){
			d2=search.Search("omim_index_csv","synonymsDiseaseName",disease);
			if (d2!=null && d==null){
				d=orph.search(d2.get(1).getName());
			}
		}


		//MySql
		sql.SQLRequest1(disease);
		ArrayList<Disease> d3= sql.setDisease(disease);

		//xml
		XMLSearch searchXML= new XMLSearch();
		ArrayList<Disease> d4= searchXML.XMLSearch(disease);


		//Final disease
		Disease finalDisease=this.Results(d, d1, d2, d3, d4);


		return finalDisease;


	}

	/** Multiples Disease requests with and
	 * 
	 * @param req: your request
	 * @return Disease
	 */
	public Disease requestAND(String req) throws Exception{
		Disease d1;
		Disease d2;
		String[] s=req.split("&&");

		d1=mapDisease(s[0].trim());
		for (int i=1; i<s.length; i++){
			d2=mapDisease(s[i].trim());
			d1=Disease.intersection(d1, d2);
		}
		return d1;
	}

	/** Multiples requests with or
	 * 
	 * @param req: your request
	 * @return Disease
	 */
	public Disease requestOR(String req) throws Exception{
		Disease d1;
		Disease d2;
		String[] s=req.split("\\|\\|");
		d1= requestAND(s[0]);
		for (int i=1; i<s.length; i++){
			d2=requestAND(s[i]);
			d1=Disease.union(d1, d2);
		}
		return d1;

	}

	/** Multiples Drug requests with and
	 * 
	 * @param req
	 * @return
	 * @throws ParseException
	 * @throws SQLException
	 */
	public Drug requestDrugAND(String req) throws ParseException, SQLException{
		Drug d1;
		Drug d2;
		String[] s=req.split("&&");
		
		d1=mapDrug(s[0].trim());
		for (int i=1; i<s.length; i++){
			d2=mapDrug(s[i].trim());
			d1=Drug.intersection(d1, d2);
		}
		return d1;
	}
	
	/**Multiples Disease requests with or
	 * 
	 * @param req
	 * @return
	 * @throws ParseException
	 * @throws SQLException
	 */
	public Drug requestDrugOR(String req) throws ParseException, SQLException{
		Drug d1;
		Drug d2;
		String[] s=req.split("\\|\\|");
		d1= requestDrugAND(s[0]);
		for (int i=1; i<s.length; i++){
			d2=requestDrugAND(s[i]);
			d1=Drug.union(d1, d2);
		}
		return d1;
		
	}
	
	/**Multiples Symptom requests with or
	 * 
	 * @param req
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 * @throws ParseException
	 */
	public ArrayList<String> requestSymptomsOR(String req) throws IOException, JSONException, ParseException{
		ArrayList<String> symptom1;
		ArrayList<String> symptom2;
		String[] s =req.split("\\|\\|");
		symptom1= requestSymptomsAND(s[0]);
		ArrayList<String> distinctList=symptom1;

		for (int i=1; i<s.length; i++){
			symptom2=requestSymptomsAND(s[i]);
			if (symptom1!=null && symptom2!=null){
				symptom1.addAll(symptom2);
			}else if (symptom1==null && symptom2!=null){
				Collections.sort(symptom2);
				return symptom2;
			}else if (symptom1!=null && symptom2==null){
				Collections.sort(symptom1);
				return symptom1;
			}else {
				return null;
			}

			Set<String> set = new HashSet<String>() ;
			set.addAll(symptom1) ;
			distinctList = new ArrayList<String>(set) ;
		}
		if (distinctList!=null){
			Collections.sort(distinctList);
		}
		return distinctList;
	}

	/**Multiples Symptom requests with and
	 * 
	 * @param req
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 * @throws ParseException
	 */
	public ArrayList<String> requestSymptomsAND(String req) throws IOException, JSONException, ParseException{
		ArrayList<String> symptom1;
		ArrayList<String> symptom2;
		String[] s=req.split("&&");
		symptom1= mapSymptoms(s[0].trim());

		for (int i=1; i<s.length; i++){
			symptom2=mapSymptoms(s[i].trim());
			if(symptom2!=null && symptom1!=null){
				symptom1.retainAll(symptom2);
			}else {
				symptom1=null;
			}


		}
		
		
		return symptom1;
	}


	public void close() throws Exception{
		sql.close();
	}
}
