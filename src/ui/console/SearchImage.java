package ui.console;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.json.JSONObject;

public class SearchImage {
	
	ArrayList<String> imagesList;
	 
	public SearchImage(String request) throws JSONException, IOException{
		imagesList= new ArrayList<String>();
		request= request.replaceAll(" ", "%20");
        String accountKey = "Wfh4LGhdOetsOmJIQ1sUG+2HY+3K8janqnZkZ3Ib26A";
        String accountKeyEnc = Base64.getEncoder().encodeToString((accountKey + ":" + accountKey).getBytes());

        URL url = new URL("https://api.datamarket.azure.com/Bing/Search/Image?Query=%27" + request + "%27&$top=10&$format=json");
        final URLConnection connection = url.openConnection();
        connection.setRequestProperty("Authorization", "Basic " + accountKeyEnc);

        try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            JSONObject json = new JSONObject(response.toString());
            JSONObject d = json.getJSONObject("d");
            JSONArray results = d.getJSONArray("results");
            int resultsLength = results.length();
            for (int i = 0; i < resultsLength; i++) {
                JSONObject aResult = results.getJSONObject(i);
                String urlImage= aResult.getString("MediaUrl");
                imagesList.add(urlImage);
            }
        }
    }

	public ArrayList<String> getImagesList() {
		return imagesList;
	}

	public void setImagesList(ArrayList<String> imagesList) {
		this.imagesList = imagesList;
	}
	
}
