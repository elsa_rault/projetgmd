package ui.console;

import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics2D;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.json.JSONException;

import Utils.Disease;
import Utils.Drug;
import ui.graphical.PlasticTabbedPaneUI;

import java.awt.GridLayout;
import java.awt.Image;
import java.awt.RenderingHints;
import java.util.ArrayList;

import javax.swing.JTabbedPane;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import java.awt.SystemColor;

public class ViewingWindow extends JFrame {

	private JPanel contentPane;
	private Boolean isDisease = false;
	private Boolean isDrug = false;
	private Boolean isSymptom = false;
	private JButton Previous;
	private JButton Next;
	private Image image;
	private JPanel imagePanel;
	private JLabel lblImage = new JLabel();
	private JPanel symptomsTab;
	private JPanel headledByTab;
	private JPanel causedByTab;
	private JPanel indicationsTab;
	private JPanel side_effectsTab;
	private JPanel diseasesTab;
	private int[] index={0, 0, 0, 0, 0, 0};
	private static ViewingWindow frame;
	private SearchImage search;
	private JTextField txtNoResults;
	private JTextField textField;
	private JButton btnNewButton;
	private JButton button;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ArrayList<String> list = new ArrayList<String>();
					list.add("fever ");
					list.add("back pain");
					frame = new ViewingWindow(list);
					frame.setVisible(true);
					frame.setResizable(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings("unchecked")
	public ViewingWindow(Object result) {
		
		Disease d = null;
		Drug drug = null;
		ArrayList<String> symptoms=null;
		
		if(result instanceof Disease){		

			d= (Disease) result;
			isDisease = true;
		}else if(result instanceof Drug){

			isDrug = true;
			drug= (Drug) result;
		}else if(result instanceof ArrayList<?>){

			isSymptom = true;
			symptoms= (ArrayList<String>) result;
		}			
		
		setVisible(true);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 870, 580);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBackground(SystemColor.menu);
		tabbedPane.setBounds(10, 11, 834, 109);
		tabbedPane.setUI(new PlasticTabbedPaneUI());
		contentPane.add(tabbedPane);
		
		imagePanel = new JPanel();
		imagePanel.setBackground(SystemColor.menu);
		imagePanel.setBounds(10, 119, 834, 411);
		contentPane.add(imagePanel);
		imagePanel.setLayout(null);
		
		if(isDisease == true){
			if (d!=null && d.getSymptoms()!=null){
				symptomsTab = new JPanel();
				symptomsTab.setBackground(Color.WHITE);
				tabbedPane.addTab("Symptoms", null, symptomsTab, null);
				int size= d.getSymptoms().size();
				String[] symptomsList = new String[size];
				for (int i = 0; i <size; i++) {
					String[] s=d.getSymptoms().get(i).getName().split(";");
					if(s[0].length()>81){
						symptomsList[i] = s[0].substring(0,80);
					}else{
						symptomsList[i] = s[0];
					}
					
				}
				JComboBox comboBox = new JComboBox(symptomsList);
				comboBox.setBounds(20, 30, 50, 30);
				symptomsTab.add(comboBox);
				
				JButton searchButton = new JButton("Search");
				searchButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						index[0]=0;
						String s=""+comboBox.getSelectedItem();
						
						ArrayList<String> url= new ArrayList<String>();
						try {
							search= new SearchImage(s);
							url= search.getImagesList();
							if(url.size()<=0){
								txtNoResults = new JTextField();
								txtNoResults.setFont(new Font("Tahoma", Font.PLAIN, 28));

								txtNoResults.setBackground(Color.WHITE);
								txtNoResults.setEnabled(false);
								txtNoResults.setEditable(false);
								txtNoResults.setForeground(Color.RED);
								txtNoResults.setText("No results ");
								txtNoResults.setBounds(139, 39, 518, 275);
								imagePanel.add(txtNoResults);
								txtNoResults.setColumns(10);
								
							}else{
								showImage(url.get(0),0,url);
								repaint();
					            validate();
							}
							
						} catch (JSONException | IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
				
				searchButton.setBounds(85, 254, 110, 41);
				symptomsTab.add(searchButton);
			}
			if (d!=null && d.getDrugs()!=null){
				headledByTab = new JPanel();
				headledByTab.setBackground(Color.WHITE);
				tabbedPane.addTab("HeadledByTab", null, headledByTab, null);
				int size= d.getDrugs().size();
				String[] drugList = new String[size];
				for (int i = 0; i <size; i++) {
					String[] s=d.getDrugs().get(i).getName().split(";");
					if(s[0].length()>81){
						drugList[i] = s[0].substring(0,80);
					} else {
						drugList[i] = s[0];
					}
					
				}
				JComboBox comboBox = new JComboBox(drugList);
				comboBox.setBounds(20, 30, 28, 20);
				headledByTab.add(comboBox);
				
				JButton searchButton = new JButton("Search");
				searchButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						String s="drug "+comboBox.getSelectedItem();
						index[1]=0;
						ArrayList<String> url= new ArrayList<String>();
						try {
							search= new SearchImage(s);
							url= search.getImagesList();
							showImage(url.get(0),1,url);
							repaint();
				            validate();
						} catch (JSONException | IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
				searchButton.setBounds(85, 254, 110, 41);
				headledByTab.add(searchButton);
			}
			if (d!=null && d.getParentsDrugs()!=null){
				causedByTab = new JPanel();
				causedByTab.setBackground(Color.WHITE);
				tabbedPane.addTab("CausedByTab", null, causedByTab, null);
				int size= d.getParentsDrugs().size();
				String[] drugList = new String[size];
				for (int i = 0; i <size; i++) {
					String[] s=d.getParentsDrugs().get(i).getName().split(";");
					if(s[0].length()>81){
						drugList[i] = s[0].substring(0,80);
					} else {
						drugList[i] = s[0];
					}
				}
				JComboBox comboBox = new JComboBox(drugList);
				comboBox.setBounds(20, 30, 28, 20);
				causedByTab.add(comboBox);
				
				JButton searchButton = new JButton("Search");
				searchButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						String s="drug "+comboBox.getSelectedItem();
						index[2]=0;
						ArrayList<String> url= new ArrayList<String>();
						try {
							search= new SearchImage(s);
							url= search.getImagesList();
							showImage(url.get(0),2,url);
							repaint();
				            validate();
						} catch (JSONException | IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
				searchButton.setBounds(85, 254, 110, 41);
				causedByTab.add(searchButton);
			}
			
		}
		else if(isDrug == true){
			if (drug!=null && drug.getIndications()!=null){
				indicationsTab = new JPanel();
				indicationsTab.setBackground(Color.WHITE);
				tabbedPane.addTab("Indications", null, indicationsTab, null);
				int size= drug.getIndications().size();
				String[] indicationsList = new String[size];
				for (int i = 0; i <size; i++) {
					String[] s=drug.getIndications().get(i).split(";");
					if(s[0].length()>81){
						indicationsList[i] = s[0].substring(0,80);

					} else {
						indicationsList[i] = s[0];

					}
				}
				JComboBox comboBox = new JComboBox(indicationsList);
				comboBox.setBounds(20, 30, 28, 20);
				indicationsTab.add(comboBox);
				
				JButton searchButton = new JButton("Search");
				searchButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						String s="drug "+comboBox.getSelectedItem();
						
						index[3]=0;
						ArrayList<String> url= new ArrayList<String>();
						try {
							search= new SearchImage(s);
							url= search.getImagesList();
							showImage(url.get(0),3,url);
							repaint();
				            validate();
						} catch (JSONException | IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
				searchButton.setBounds(85, 254, 110, 41);
				indicationsTab.add(searchButton);
			}
			if (drug!=null && drug.getSide_effect()!=null){
				side_effectsTab = new JPanel();
				side_effectsTab.setBackground(Color.WHITE);
				tabbedPane.addTab("Side effects", null, side_effectsTab, null);
				int size= drug.getSide_effect().size();
				String[] sideEffectList = new String[size];
				for (int i = 0; i <size; i++) {
					String[] s= drug.getSide_effect().get(i).split(";");
					if(s[0].length()>81){
						sideEffectList[i] = s[0].substring(0,80);

					}else {
						sideEffectList[i] = s[0];

					}
				}
				JComboBox comboBox = new JComboBox(sideEffectList);
				comboBox.setBounds(20, 30, 28, 20);
				side_effectsTab.add(comboBox);
				
				JButton searchButton = new JButton("Search");
				searchButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						String s="drug "+comboBox.getSelectedItem();
						index[4]=0;
						ArrayList<String> url= new ArrayList<String>();
						try {
							search= new SearchImage(s);
							url= search.getImagesList();
							showImage(url.get(0),4,url);
							repaint();
				            validate();
						} catch (JSONException | IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
				searchButton.setBounds(85, 254, 110, 41);
				side_effectsTab.add(searchButton);
			}
			
		}
		else if(isSymptom == true){
			if (symptoms!=null){
				diseasesTab = new JPanel();
				diseasesTab.setBackground(Color.WHITE);
				tabbedPane.addTab("Diseases", null, diseasesTab, null);
				int size= symptoms.size();
				String[] diseaseList = new String[size];
				for (int i = 0; i <size; i++) {
					String[] s= symptoms.get(i).split(";");
					if(s[0].length()>81){
						diseaseList[i] = s[0].substring(0,80);
					} else {
						diseaseList[i] = s[0];

					}
				}
				JComboBox comboBox = new JComboBox(diseaseList);
				comboBox.setBounds(20, 90, 28, 20);
				diseasesTab.add(comboBox);
				
				JButton searchButton = new JButton("Search");
				searchButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						if(comboBox.getSelectedIndex() > -1){
							
							String s=""+comboBox.getSelectedItem();
							index[5]=0;
							ArrayList<String> url= new ArrayList<String>();
							try {
								search= new SearchImage(s);
								url= search.getImagesList();
								showImage(url.get(0),5,url);
								repaint();
					            validate();
							} catch (JSONException | IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
						}
						
					}
				});
				searchButton.setBounds(85, 254, 110, 41);
				diseasesTab.add(searchButton);
			}
			
		}
	}
	
	public void showButton(int i, ArrayList<String> list){
		Previous = new JButton("Previous");
		Previous.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (index[i]>0){
					index[i]--;
					showImage(list.get(index[i]),i,list);
					repaint();
		            validate();
				}
							
			}
		});
		Previous.setFont(new Font("MV Boli", Font.BOLD, 17));
		Previous.setBounds(78, 339, 126, 43);
		imagePanel.add(Previous);
		
		Next = new JButton("Next");
		Next.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if ((index[i]+1)<5 && index[i]<(list.size()-1)){
					index[i]++;
					
					showImage(list.get(index[i]),i,list);
					repaint();
		            validate();
				}
							
			}
		});
		Next.setFont(new Font("MV Boli", Font.BOLD, 17));
		Next.setBounds(631, 339, 126, 43);
		imagePanel.add(Next);
	}
	
	public void showImage(String url2,int i, ArrayList<String> list){
        try {
        	if (imagePanel!=null){
        		remove(imagePanel);
                validate();
                repaint();
        	}
        	
            imagePanel = new JPanel();
    		imagePanel.setBackground(SystemColor.menu);
    		imagePanel.setBounds(10, 119, 834, 411);
    		contentPane.add(imagePanel);
    		imagePanel.setLayout(null);
            URL url = new URL(url2);
            image = ImageIO.read(url);
            if (image!=null){
            	image = image.getScaledInstance(300, 300,Image.SCALE_DEFAULT);
                
                lblImage =  new JLabel(new ImageIcon(image));
                lblImage.setBounds(139, 39, 518, 275);
                imagePanel.add(lblImage);
                showButton(i,list);
                repaint();
                validate();
            }else {
            	txtNoResults = new JTextField();
				txtNoResults.setFont(new Font("Tahoma", Font.PLAIN, 28));
				txtNoResults.setForeground(Color.RED);
				txtNoResults.setBackground(Color.WHITE);
				txtNoResults.setEnabled(false);
				txtNoResults.setEditable(false);
				txtNoResults.setText("Error Image not found");
				txtNoResults.setBounds(139, 39, 518, 275);
				imagePanel.add(txtNoResults);
				txtNoResults.setColumns(10);
            	showButton(i,list);
                repaint();
                validate();
            }
            
            
        } catch (IOException e) {
        	txtNoResults = new JTextField();
			txtNoResults.setFont(new Font("Tahoma", Font.PLAIN, 28));
			txtNoResults.setForeground(Color.RED);
			txtNoResults.setBackground(Color.WHITE);
			txtNoResults.setEnabled(false);
			txtNoResults.setEditable(false);
			txtNoResults.setText("Error Image not found");
			txtNoResults.setBounds(139, 39, 518, 275);
			imagePanel.add(txtNoResults);
			txtNoResults.setColumns(10);
        	showButton(i,list);
            repaint();
            validate();
        }        
    }
}
