package ui.console;

import java.util.ArrayList;
import java.util.Scanner;

import Utils.Disease;
import Utils.Drug;

public class Main {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		Mapping mapping= new Mapping();
		boolean drug=false;
		boolean disease=false;
		boolean symptoms=false;
		boolean quit=false;
		boolean correct=false;
		Scanner sc = new Scanner(System.in);
		String str;
		while (!quit){
			while (!correct){
				System.out.println("Please choose one field or q for quit:");
				System.out.println("1-Disease");
				System.out.println("2-Drug");
				System.out.println("3-Symptoms");
				str=sc.nextLine();
				if (str.equals("1") || str.equals("2") || str.equals("q") || str.equals("3")){
					correct=true;
					if (str.equals("q")){
						quit=true;
						break;
					}
					if (str.equals("1")){
						disease=true;
					}
					if (str.equals("2")){
						drug=true;
					}
					if (str.equals("3")){
						symptoms=true;
					}

				}
				else {
					System.out.println(str+" is not a rigth answer. Please choose between 1, 2 or 3.");
				}
			}
			if (quit){
				break;
			}
			correct=false;
			String request = null;
			if (disease){
				request="diseases";
			}
			if (drug){
				request="drugs";
			}
			if (symptoms){
				request="symptoms";
			}
			System.out.println("Please enter "+request+":");
			str=sc.nextLine();
			while (str.equals("")){
				System.out.println("please enter a word");
				str=sc.nextLine();
			}
			if (disease){
				Disease  dis=mapping.requestOR(str);
				if (dis!=null){
					dis.print();
				}else{
					System.out.println("no results");
				}
				disease=false;
			}else if (drug){
				Drug d=mapping.requestDrugOR(str);
				if (d!=null){
					Mapping.printDrug(d);
				}else {
					System.out.println("no results");
				}
				drug=false;
			}else if (symptoms){
				ArrayList<String> list=mapping.requestSymptomsOR(str);
				if (list!= null && !list.isEmpty()){
					Mapping.printSymptom(str,list);
				}else {
					System.out.print("no results");
				}
				symptoms=false;
			}


			
		}
		mapping.close();
	}

}
