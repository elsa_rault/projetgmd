package ui.exportcsv;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import Utils.Disease;
import Utils.Drug;

import java.text.*;
import java.lang.*;

/**
 * This class is used to export data in CSV files
 * 
 */
public class ExportCSVDisease {


	private String COMA_DELIMITER = ";";
	private String NEW_LINE_SEPARATOR ="\n";
	private String FILE_HEADER ="Synonyms; Symptoms; Healed by; Caused by";


	/**
	 * Constructor of the class, used to create the CSV file
	 * 
	 * @param filePath
	 * @param disease
	 * @throws IOException
	 */
	public ExportCSVDisease(String filePath, Disease disease) throws IOException{

		StringBuffer sbf=new StringBuffer();
		File file= new File(filePath+".csv");
		FileWriter fileWriter= new FileWriter(file);
		if (file.exists()){
			file.delete();
		}
		sbf.append(FILE_HEADER);

		int sizeText=0;
		int sizeSymptoms=0;
		int sizeHealed=0;
		int sizeCaused=0;
		
		
		String text="";
		if(disease.getSynonyms() != null){
			sizeText=disease.getSynonyms().size();
		}

		String textSymptoms="";
		if(disease.getSymptoms()!=null){
			sizeSymptoms=disease.getSymptoms().size();
		}

		String textHealed="";
		if(disease.getDrugs()!= null){
			sizeHealed=disease.getDrugs().size();
		}

		String textCaused="";
		if(disease.getParentsDrugs()!= null){
			sizeCaused=disease.getParentsDrugs().size();
		}
		int size=Math.max(sizeText, sizeSymptoms);
		size=Math.max(size,sizeHealed);
		size=Math.max(size,sizeCaused);
		
		for (int i=0; i<size; i++){
			sbf.append(NEW_LINE_SEPARATOR);
			if(i<sizeText){
				text=disease.getSynonyms().get(i);
				text=text.replaceAll(";",",");
			}else{
				text="";
			}
			sbf.append(text);
			sbf.append(COMA_DELIMITER);
			if(i<sizeSymptoms){
				textSymptoms=disease.getSymptoms().get(i).getName();
				textSymptoms=textSymptoms.replaceAll(";",",");
			}else{
				textSymptoms="";
			}
			sbf.append(textSymptoms);
			sbf.append(COMA_DELIMITER);
			if(i<sizeHealed){
				textHealed=disease.getDrugs().get(i).getName();
				textHealed=textHealed.replaceAll(";",",");
			}else{
				textHealed="";
			}
			sbf.append(textHealed);
			sbf.append(COMA_DELIMITER);
			if(i<sizeCaused){
				textCaused=disease.getParentsDrugs().get(i).getName();
				textCaused=textCaused.replaceAll(";",",");
			}else{
				textCaused="";
			}
			sbf.append(textCaused); 
			sbf.append(COMA_DELIMITER);
		}
		BufferedWriter bwr = new BufferedWriter(fileWriter);
		bwr.write(sbf.toString());
		bwr.flush();
		bwr.close();
		System.out.println("fichier csv cr��");

	}





}