package ui.exportcsv;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import Utils.Disease;
import Utils.Drug;

import java.text.*;
import java.lang.*;

/**
 * This class is used to export data in CSV files
 * 
 */
public class ExportCSVDrug {
	private String COMA_DELIMITER = ";";
	private String NEW_LINE_SEPARATOR ="\n";
	private String FILE_HEADER ="Indications; Side Effects";
	
	
	/**
	 * Constructor of the class, used to create the CSV file
	 * 
	 * @param filePath
	 * @param drug
	 * @throws IOException
	 */
	public ExportCSVDrug(String filePath, Drug drug) throws IOException{
		
		StringBuffer sbf=new StringBuffer();
		File file= new File(filePath+".csv");
		FileWriter fileWriter= new FileWriter(file);
		if (file.exists()){
			file.delete();
		}
		sbf.append(FILE_HEADER);
		
		int sizeText=0;
		int sizeSide=0;
		
		
		String text="";
		if(drug.getIndications() != null){
			sizeText=drug.getIndications().size();
		}
		
		String textSideEffects="";
		if(drug.getSide_effect()!=null){
			sizeSide=drug.getSide_effect().size();
		}
			
		int size=Math.max(sizeText, sizeSide);
		
		for (int i=0; i<size; i++){
			sbf.append(NEW_LINE_SEPARATOR);
			if(i<sizeText){
				text=drug.getIndications().get(i);
				text=text.replaceAll(";",",");
			}else{
				text="";
			}
			sbf.append(text);
			sbf.append(COMA_DELIMITER);
			if(i<sizeSide){
				textSideEffects=drug.getSide_effect().get(i);
				textSideEffects=textSideEffects.replaceAll(";",",");
			}else{
				textSideEffects="";
			}
			sbf.append(textSideEffects);
			sbf.append(COMA_DELIMITER);
			
		}
			
		
		BufferedWriter bwr = new BufferedWriter(fileWriter);
		bwr.write(sbf.toString());
		bwr.flush();
		bwr.close();
		System.out.println("fichier csv cr��");
		
	}
	

}
