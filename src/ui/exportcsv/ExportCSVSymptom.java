package ui.exportcsv;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import Utils.Disease;
import Utils.Drug;

import java.text.*;
import java.lang.*;

/**
 * This class is used to export data in CSV files
 * 
 */
public class ExportCSVSymptom {
	private String COMA_DELIMITER = ";";
	private String NEW_LINE_SEPARATOR ="\n";
	private String FILE_HEADER ="Disease";
	
	
	/**
	 * Constructor of the class, used to create the CSV file
	 * 
	 * @param filePath
	 * @param symptom
	 * @throws IOException
	 */
public ExportCSVSymptom(String filePath, ArrayList<String> symptom) throws IOException{
		
		StringBuffer sbf=new StringBuffer();
		File file= new File(filePath+".csv");
		FileWriter fileWriter= new FileWriter(file);
		if (file.exists()){
			file.delete();
		}
		sbf.append(FILE_HEADER);
		
		
		String text="";
		if(symptom != null){
		for(int i=0; i<symptom.size(); i++){
			text = text +symptom.get(i)+ "\n";
		}
		}else{
			text=" ";
		}
			
			sbf.append(NEW_LINE_SEPARATOR);
			sbf.append(text);
			sbf.append(COMA_DELIMITER);
		
		BufferedWriter bwr = new BufferedWriter(fileWriter);
		bwr.write(sbf.toString());
		bwr.flush();
		bwr.close();
		System.out.println("fichier csv cr��");
		
	}
	

}
