package ui.graphical;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import Utils.Disease;
import Utils.Drug;
import ui.console.Mapping;
import ui.console.ViewingWindow;
import ui.exportcsv.ExportCSVDisease;
import ui.exportcsv.ExportCSVDrug;
import ui.exportcsv.ExportCSVSymptom;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JTabbedPane;
import javax.swing.border.LineBorder;

import java.awt.Font;
import javax.swing.JList;
import javax.swing.JMenuItem;

/**
 * This class allows to display the result of the search
 * 
 */
public class ResultsWindow extends JFrame{
	private final JTabbedPane tabbedPane =  new JTabbedPane(JTabbedPane.TOP);
	Boolean isDisease = false;
	Boolean isDrug = false;
	Boolean isSymptom = false;

	/**
	 * Constructor of the class
	 * 
	 * @param disease
	 * @param drug
	 * @param symtom
	 * @param mapping
	 * @param str
	 * @param str1
	 * @param checked
	 */
	public ResultsWindow(Disease disease, Drug drug, ArrayList<String> symptom, Mapping mapping, String str, String str1, int checked){

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100,100,1220,620);
		Font font1 = new Font("Nirmala UI Semilight", Font.PLAIN, 17);

		if(checked == 0)
			isDisease = true;
		else if(checked == 1)
			isDrug = true;
		else 
			isSymptom = true;

		getContentPane().setLayout(null);
		tabbedPane.setBounds(0, 0, 1202, 297);
		tabbedPane.setUI(new PlasticTabbedPaneUI());
		getContentPane().add(tabbedPane);

		if(isDisease == true){
			JPanel panel = new JPanel();
			tabbedPane.addTab("Synonyms", null, panel, null);
			panel.setLayout(null);

			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(0, 0, 1195, 245);
			panel.add(scrollPane);

			JTextArea textArea = new JTextArea();
			scrollPane.setViewportView(textArea);

			JScrollPane scrollPane2 = new JScrollPane();
			scrollPane2.setBounds(20, 355, 270, 106);
			getContentPane().add(scrollPane2);

			if (disease!=null){
				String[] namesList = disease.getName().split("\\|\\|");
				String textNames = "";
				for(int j =0; j<namesList.length; j++){
					textNames = textNames + namesList[j].trim()+"\n";
				}
				JTextArea name = new JTextArea();
				scrollPane2.setViewportView(name);
				name.setText(textNames);
				name.setFont(font1);

			}

			if (isDisease == true){
				if(disease != null && disease.getSynonyms() != null){
					String text="";
					int nb = 0;
					Collections.sort(disease.getSynonyms());
					for(int i=0; i<disease.getSynonyms().size(); i++){
						if (disease.getSynonyms().get(i)!=null && !disease.getSynonyms().get(i).trim().isEmpty()){
							text = text +disease.getSynonyms().get(i).trim()+ "\n";
							nb = nb+1;
						}

					}
					textArea.setText(text);
					textArea.setFont(font1);

					JLabel lblNewLabel_5 = new JLabel("Synonyms : "+ nb +" results found");
					lblNewLabel_5.setFont(new Font("MV Boli", Font.PLAIN, 17));
					lblNewLabel_5.setBounds(326, 368, 264, 31);
					getContentPane().add(lblNewLabel_5);
				}
				else{
					textArea.setText("No results found");
					textArea.setFont(font1);

					JLabel lblNewLabel_5 = new JLabel("Synonyms : 0 results found");
					lblNewLabel_5.setFont(new Font("MV Boli", Font.PLAIN, 17));
					lblNewLabel_5.setBounds(326, 368, 264, 31);
					getContentPane().add(lblNewLabel_5);
				}
			}

			JPanel panel_1 = new JPanel();
			tabbedPane.addTab("Symptoms", null, panel_1, null);
			panel_1.setLayout(null);

			JScrollPane scrollPane_1 = new JScrollPane();
			scrollPane_1.setBounds(0, 0, 1195, 245);
			panel_1.add(scrollPane_1);

			JTextArea textArea_1 = new JTextArea();
			scrollPane_1.setViewportView(textArea_1);
			if (isDisease == true){
				if(disease != null && disease.getSymptoms()!= null){
					String text="";
					int nb = 0;
					Disease.sortDisease(disease.getSymptoms());
					for(int i=0; i<disease.getSymptoms().size(); i++){
						if (disease.getSymptoms().get(i).getName()!=null && !disease.getSymptoms().get(i).getName().trim().isEmpty()){
							text = text +disease.getSymptoms().get(i).getName().trim()+ "\n";
							nb = nb+1;
						}					
					}
					textArea_1.setText(text);
					textArea_1.setFont(font1);


					JLabel lblNewLabel_6 = new JLabel("Symptoms : "+ nb +" results found");
					lblNewLabel_6.setFont(new Font("MV Boli", Font.PLAIN, 17));
					lblNewLabel_6.setBounds(326, 412, 264, 28);
					getContentPane().add(lblNewLabel_6);
				}
				else{
					textArea_1.setText("No results found");
					textArea_1.setFont(font1);


					JLabel lblNewLabel_6 = new JLabel("Symptoms : 0 results found");
					lblNewLabel_6.setFont(new Font("MV Boli", Font.PLAIN, 17));
					lblNewLabel_6.setBounds(326, 412, 264, 28);
					getContentPane().add(lblNewLabel_6);
				}
			}

			JPanel panel_2 = new JPanel();
			tabbedPane.addTab("Healed by", null, panel_2, null);
			panel_2.setLayout(null);

			JScrollPane scrollPane_2 = new JScrollPane();
			scrollPane_2.setBounds(0, 0, 1195, 245);
			panel_2.add(scrollPane_2);

			JTextArea textArea_2 = new JTextArea();
			scrollPane_2.setViewportView(textArea_2);
			if (isDisease == true){
				if(disease != null && disease.getDrugs() != null){
					String text="";
					int nb = 0;
					disease.setDrugs(Drug.sortDrug(disease.getDrugs()));
					for(int i=0; i<disease.getDrugs().size(); i++){
						if (disease.getDrugs().get(i)!=null && !disease.getDrugs().get(i).getName().trim().isEmpty()){
							text = text +disease.getDrugs().get(i)+ "\n";
							nb = nb+1;
						}

					}
					textArea_2.setText(text);
					textArea_2.setFont(font1);

					JLabel lblNewLabel_7 = new JLabel("Healed by : "+ nb +" results found");
					lblNewLabel_7.setFont(new Font("MV Boli", Font.PLAIN, 17));
					lblNewLabel_7.setBounds(618, 368, 280, 31);
					getContentPane().add(lblNewLabel_7);
				}
				else{
					textArea_2.setText("No results found");
					textArea_2.setFont(font1);
					JLabel lblNewLabel_7 = new JLabel("Healed by : 0 results found");
					lblNewLabel_7.setFont(new Font("MV Boli", Font.PLAIN, 17));
					lblNewLabel_7.setBounds(618, 368, 280, 31);
					getContentPane().add(lblNewLabel_7);
				}
			}

			JPanel panel_3 = new JPanel();
			tabbedPane.addTab("Caused by", null, panel_3, null);
			panel_3.setLayout(null);

			JScrollPane scrollPane_5 = new JScrollPane();
			scrollPane_5.setBounds(0, 0, 1195, 245);
			panel_3.add(scrollPane_5);

			JTextArea textArea_3 = new JTextArea();
			scrollPane_5.setViewportView(textArea_3);
			if(isDisease == true){
				if(disease != null && disease.getParentsDrugs() != null){
					String text="";
					int nb = 0;
					disease.setParentsDrugs(Drug.sortDrug(disease.getParentsDrugs()));
					for(int i=0; i<disease.getParentsDrugs().size(); i++){
						if (disease.getParentsDrugs().get(i)!=null && !disease.getParentsDrugs().get(i).getName().isEmpty()){
							text = text +disease.getParentsDrugs().get(i)+ "\n";
							nb =nb+1;
						}

					}
					textArea_3.setText(text);
					textArea_3.setFont(font1);


					JLabel lblNewLabel_8 = new JLabel("Caused by : "+ nb +" results found");
					lblNewLabel_8.setFont(new Font("MV Boli", Font.PLAIN, 17));
					lblNewLabel_8.setBounds(618, 412, 291, 28);
					getContentPane().add(lblNewLabel_8);
				}
				else{
					textArea_3.setText("No results found");
					textArea_3.setFont(font1);

					JLabel lblNewLabel_8 = new JLabel("Caused by : 0 results found");
					lblNewLabel_8.setFont(new Font("MV Boli", Font.PLAIN, 17));
					lblNewLabel_8.setBounds(618, 412, 291, 28);
					getContentPane().add(lblNewLabel_8);
				}
			}
		}

		if(isDrug == true){
			JPanel panel_4 = new JPanel();
			tabbedPane.addTab("Indications", null, panel_4, null);
			panel_4.setLayout(null);

			JScrollPane scrollPane_6 = new JScrollPane();
			scrollPane_6.setBounds(0, 0, 1195, 245);
			panel_4.add(scrollPane_6);

			JTextArea textArea_4 = new JTextArea();
			scrollPane_6.setViewportView(textArea_4);

			JScrollPane scrollPane2 = new JScrollPane();
			scrollPane2.setBounds(20, 355, 270, 106);
			getContentPane().add(scrollPane2);

			String[] namesList = drug.getName().split("\\|\\|");
			String textNames = "";
			for(int j =0; j<namesList.length; j++){
				textNames = textNames + namesList[j]+"\n";
			}
			JTextArea name = new JTextArea();
			scrollPane2.setViewportView(name);
			name.setText(textNames);
			name.setFont(font1);

			if(isDrug == true){
				if(drug != null && drug.getIndications() != null){
					String text="";
					int nb = 0;
					Collections.sort(drug.getIndications());
					for(int i=0; i<drug.getIndications().size(); i++){
						if (drug.getIndications().get(i)!=null &&  !drug.getIndications().get(i).trim().isEmpty()){
							text = text +drug.getIndications().get(i).trim()+" "+drug.getSources()+ "\n";
							nb = nb+1;
						}
					}
					textArea_4.setText(text);
					textArea_4.setFont(font1);

					JLabel lblNewLabel_5 = new JLabel("Indications : "+ nb +" results found");
					lblNewLabel_5.setFont(new Font("MV Boli", Font.PLAIN, 17));
					lblNewLabel_5.setBounds(326, 368, 264, 31);
					getContentPane().add(lblNewLabel_5);
				}
				else{
					textArea_4.setText("No results found");
					textArea_4.setFont(font1);

					JLabel lblNewLabel_5 = new JLabel("Indications : 0 results found");
					lblNewLabel_5.setFont(new Font("MV Boli", Font.PLAIN, 17));
					lblNewLabel_5.setBounds(326, 368, 264, 31);
					getContentPane().add(lblNewLabel_5);
				}
			}

			JPanel panel_5 = new JPanel();
			tabbedPane.addTab("Side effects", null, panel_5, null);
			panel_5.setLayout(null);

			JScrollPane scrollPane_3 = new JScrollPane();
			scrollPane_3.setBounds(0, 0, 1195, 245);
			panel_5.add(scrollPane_3);

			JTextArea textArea_5 = new JTextArea();
			scrollPane_3.setViewportView(textArea_5);
			if(isDrug == true){
				if(drug != null && drug.getSide_effect() != null){
					String text="";
					int nb = 0;
					Collections.sort(drug.getSide_effect());
					for(int i=0; i<drug.getSide_effect().size(); i++){
						if (drug.getSide_effect().get(i)!=null && !drug.getSide_effect().get(i).trim().isEmpty()){
							text = text +drug.getSide_effect().get(i).trim()+" "+drug.getSources()+ "\n";
							nb=nb+1;
						}

					}
					textArea_5.setText(text);
					textArea_5.setFont(font1);


					JLabel lblNewLabel_6 = new JLabel("Side effects : "+ nb +" results found");
					lblNewLabel_6.setFont(new Font("MV Boli", Font.PLAIN, 17));
					lblNewLabel_6.setBounds(326, 412, 264, 28);
					getContentPane().add(lblNewLabel_6);
				}
			}
			else{
				textArea_5.setText("No results found");
				textArea_5.setFont(font1);


				JLabel lblNewLabel_6 = new JLabel("Side effects : 0 results found");
				lblNewLabel_6.setFont(new Font("MV Boli", Font.PLAIN, 17));
				lblNewLabel_6.setBounds(326, 412, 264, 28);
				getContentPane().add(lblNewLabel_6);
			}
		}

		if(isSymptom == true){
			JPanel panel_6 = new JPanel();
			tabbedPane.addTab("Diseases", null, panel_6, null);
			panel_6.setLayout(null);
			panel_6.setBounds(0, 0, 1902, 657);

			JScrollPane scrollPane_4 = new JScrollPane();
			scrollPane_4.setBounds(0, 0, 1195, 245);
			panel_6.add(scrollPane_4);

			JTextArea textArea_6 = new JTextArea();
			scrollPane_4.setViewportView(textArea_6);


			if(isSymptom == true){
				if(symptom != null){
					String text="";
					int nb = 0;
					Collections.sort(symptom);
					for(int i=0; i<symptom.size(); i++){
						if (symptom.get(i)!=null && !symptom.get(i).trim().isEmpty()){
							text = text +symptom.get(i).trim()+ "\n";
							nb=nb+1;
						}
					}
					textArea_6.setText(text);
					textArea_6.setFont(font1);

					JLabel lblNewLabel_5 = new JLabel("Diseases : "+ nb +" results found");
					lblNewLabel_5.setFont(new Font("MV Boli", Font.PLAIN, 17));
					lblNewLabel_5.setBounds(326, 368, 264, 31);
					getContentPane().add(lblNewLabel_5);
				}
				else{
					textArea_6.setText("No results found");
					textArea_6.setFont(font1);

					JLabel lblNewLabel_5 = new JLabel("Diseases : 0 results found");
					lblNewLabel_5.setFont(new Font("MV Boli", Font.PLAIN, 17));
					lblNewLabel_5.setBounds(326, 368, 264, 31);
					getContentPane().add(lblNewLabel_5);
				}
			}

		}

		JButton btnNewButton = new JButton("New search");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GUI gui=new GUI(mapping);
				gui.setVisible(true);
				dispose();
			}
		});
		btnNewButton.setFont(new Font("MV Boli", Font.BOLD, 17));
		btnNewButton.setForeground(new Color(102, 204, 0));
		btnNewButton.setBounds(989, 323, 179, 50);
		getContentPane().add(btnNewButton);

		JLabel lblNewLabel = new JLabel("Search name :");
		lblNewLabel.setFont(new Font("MV Boli", Font.BOLD, 17));
		lblNewLabel.setForeground(new Color(0, 204, 153));
		lblNewLabel.setBounds(10, 532, 133, 28);
		getContentPane().add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("search");
		lblNewLabel_1.setBounds(155, 530, 699, 31);
		getContentPane().add(lblNewLabel_1);
		lblNewLabel_1.setText(str);
		lblNewLabel_1.setFont(font1);

		JLabel lblNewLabel_2 = new JLabel("Your results : ");
		lblNewLabel_2.setForeground(new Color(0, 0, 153));
		lblNewLabel_2.setFont(new Font("MV Boli", Font.BOLD, 17));
		lblNewLabel_2.setBounds(10, 310, 133, 41);
		getContentPane().add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("Your search : ");
		lblNewLabel_3.setForeground(new Color(51, 102, 204));
		lblNewLabel_3.setFont(new Font("MV Boli", Font.BOLD, 17));
		lblNewLabel_3.setBounds(10, 485, 133, 34);
		getContentPane().add(lblNewLabel_3);


		JLabel lblNewLabel_4 = new JLabel("");
		lblNewLabel_4.setBounds(155, 489, 739, 28);
		getContentPane().add(lblNewLabel_4);
		lblNewLabel_4.setText(str1);
		lblNewLabel_4.setFont(font1);

		JButton btnNewButton_1 = new JButton("Export to csv");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setDialogTitle("Choisissez le dossier de sauvegarde");
				fileChooser.resetChoosableFileFilters();
				FileExtension png = new FileExtension(".csv", ".csv");
				fileChooser.addChoosableFileFilter(png);
				fileChooser.setFileFilter(png);


				int userSelection = fileChooser.showSaveDialog(getContentPane());
				if (userSelection == JFileChooser.APPROVE_OPTION) {
					File fileToSave = fileChooser.getSelectedFile();

					if(isDisease ==true){
						try {
							ExportCSVDisease dd = new ExportCSVDisease(fileToSave.getAbsolutePath(), disease);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}else if(isDrug == true){
						try {
							ExportCSVDrug dd = new ExportCSVDrug(fileToSave.getAbsolutePath(), drug);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					}
					else if(isSymptom == true){
						try {
							ExportCSVSymptom dd = new ExportCSVSymptom(fileToSave.getAbsolutePath(), symptom);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}
			}
		});
		btnNewButton_1.setFont(new Font("MV Boli", Font.BOLD, 17));
		btnNewButton_1.setForeground(new Color(138, 43, 226));
		btnNewButton_1.setBounds(989, 399, 179, 50);
		getContentPane().add(btnNewButton_1);

		JButton pictures = new JButton("Pictures");
		pictures.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ViewingWindow view;
				if (isDisease){
					System.out.println("disease");
					view = new ViewingWindow(disease);

				}else if (isDrug){
					System.out.println("drug");
					view = new ViewingWindow(drug);

				}else if (isSymptom){
					System.out.println("symptom");
					view= new ViewingWindow(symptom);

				}

			}
		});
		pictures.setForeground(Color.ORANGE);
		pictures.setFont(new Font("MV Boli", Font.BOLD, 17));
		pictures.setBounds(989, 474, 179, 50);
		getContentPane().add(pictures);


	}
}
