package ui.graphical;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;

import org.json.JSONException;

import Utils.Drug;
import ui.console.Mapping;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Label;

/**
 * This class allows to display the search window
 * 
 */
public class GUI  extends JFrame {

	private JPanel contentPane;
	private JTextField textField_Drug;
	private JTextField textField_Disease;
	private JTextField collectName;
	private JButton btnNewButton;
	private JButton btnValider;
	private JLabel lblRentrezLeHashtag;
	private JLabel lblRentrezLaRfrence;
	private JLabel lblRentrezLesSymptoms;
	private JTextField textField;
	
	
	/**
  	 * Constructor of the class
	 * 
	 * @param mapping
	 */
	public GUI(Mapping mapping){
		
		setTitle("New Search");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 641, 629);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSlectionnezUnOu = new JLabel("Select the item that you want to search");
		lblSlectionnezUnOu.setForeground(new Color(0, 0, 153));
		lblSlectionnezUnOu.setFont(new Font("MV Boli", Font.PLAIN, 22));
		lblSlectionnezUnOu.setBounds(12, 149, 581, 22);
		contentPane.add(lblSlectionnezUnOu);
		
		JLabel lblChoisissezUnNom = new JLabel("Choose one which doesn't already exist!");
		lblChoisissezUnNom.setFont(new Font("MV Boli", Font.PLAIN, 13));
		lblChoisissezUnNom.setForeground(Color.RED);
		lblChoisissezUnNom.setBounds(12, 90, 308, 16);
		contentPane.add(lblChoisissezUnNom);
		lblChoisissezUnNom.setVisible(false);
		
		JLabel lblChoisissezLeNom = new JLabel("Choose the name of your new search");
		lblChoisissezLeNom.setForeground(new Color(0, 0, 153));
		lblChoisissezLeNom.setFont(new Font("MV Boli", Font.PLAIN, 22));
		lblChoisissezLeNom.setBounds(12, 13, 478, 22);
		contentPane.add(lblChoisissezLeNom);
		
		collectName = new JTextField();
		collectName.setFont(new Font("MV Boli", Font.PLAIN, 13));
		collectName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				lblChoisissezUnNom.setVisible(false);
			}
		});
		collectName.setBounds(12, 48, 534, 37);
		contentPane.add(collectName);
		collectName.setColumns(10);
		
		btnNewButton = new JButton("Cancel");
		btnNewButton.setForeground(new Color(255, 0, 0));
		btnNewButton.setFont(new Font("MV Boli", Font.BOLD, 17));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnNewButton.setBounds(483, 522, 128, 48);
		contentPane.add(btnNewButton);
		
		lblRentrezLeHashtag = new JLabel("Write the name of the disease you search!");
		lblRentrezLeHashtag.setFont(new Font("MV Boli", Font.PLAIN, 13));
		lblRentrezLeHashtag.setForeground(Color.RED);
		lblRentrezLeHashtag.setBounds(178, 246, 387, 16);
		lblRentrezLeHashtag.setVisible(false);
		contentPane.add(lblRentrezLeHashtag);
		
		lblRentrezLaRfrence = new JLabel("Write the name of the drug you search!");
		lblRentrezLaRfrence.setFont(new Font("MV Boli", Font.PLAIN, 13));
		lblRentrezLaRfrence.setForeground(Color.RED);
		lblRentrezLaRfrence.setBounds(178, 313, 387, 16);
		lblRentrezLaRfrence.setVisible(false);
		contentPane.add(lblRentrezLaRfrence);
		
		lblRentrezLesSymptoms = new JLabel("Write the name of the symptom you search!");
		lblRentrezLesSymptoms.setFont(new Font("MV Boli", Font.PLAIN, 13));
		lblRentrezLesSymptoms.setForeground(Color.RED);
		lblRentrezLesSymptoms.setBounds(178, 389, 387, 16);
		lblRentrezLesSymptoms.setVisible(false);
		contentPane.add(lblRentrezLesSymptoms);
		
		textField_Drug = new JTextField();
		textField_Drug.setFont(new Font("MV Boli", Font.PLAIN, 17));
		textField_Drug.setEnabled(false);
		textField_Drug.setVisible(false);
		textField_Drug.setBounds(178, 271, 351, 37);
		contentPane.add(textField_Drug);
		textField_Drug.setColumns(10);
		
		textField_Disease = new JTextField();
		textField_Disease.setFont(new Font("MV Boli", Font.PLAIN, 17));
		textField_Disease.setEnabled(false);
		textField_Disease.setVisible(false);
		textField_Disease.setBounds(178, 203, 351, 37);
		contentPane.add(textField_Disease);
		textField_Disease.setColumns(10);
		
		textField = new JTextField();
		textField.setFont(new Font("MV Boli", Font.PLAIN, 17));
		textField.setEnabled(false);
		textField.setVisible(false);
		textField.setColumns(10);
		textField.setBounds(178, 342, 351, 37);
		contentPane.add(textField);
		
		JRadioButton rdbtnDrug = new JRadioButton("Drug");
		rdbtnDrug.setForeground(new Color(0, 153, 255));
		rdbtnDrug.setFont(new Font("MV Boli", Font.PLAIN, 20));
		rdbtnDrug.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField_Drug.setVisible(true);
				textField_Drug.setEnabled(true);
				textField_Disease.setVisible(false);
				textField_Disease.setEnabled(false);
				textField.setVisible(false);
				textField.setEnabled(false);
			}
		});
		
		JRadioButton rdbtnDisease = new JRadioButton("Disease");
		rdbtnDisease.setForeground(new Color(51, 102, 204));
		rdbtnDisease.setFont(new Font("MV Boli", Font.PLAIN, 20));
		rdbtnDisease.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textField_Disease.setVisible(true);
				textField_Disease.setEnabled(true);
				textField_Drug.setVisible(false);
				textField_Drug.setEnabled(false);
				textField.setVisible(false);
				textField.setEnabled(false);
			}
		});
		rdbtnDisease.setBounds(34, 208, 109, 23);
		contentPane.add(rdbtnDisease);
		rdbtnDrug.setBounds(34, 276, 109, 23);
		contentPane.add(rdbtnDrug);
		
		JRadioButton rdbtnSymptoms = new JRadioButton("Symptoms");
		rdbtnSymptoms.setForeground(new Color(0, 204, 255));
		rdbtnSymptoms.setFont(new Font("MV Boli", Font.PLAIN, 20));
		rdbtnSymptoms.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setVisible(true);
				textField.setEnabled(true);
				textField_Drug.setVisible(false);
				textField_Drug.setEnabled(false);
				textField_Disease.setVisible(false);
				textField_Disease.setEnabled(false);
			}
		});
		rdbtnSymptoms.setBounds(37, 347, 136, 23);
		contentPane.add(rdbtnSymptoms);
		
		ButtonGroup bg = new ButtonGroup();
		bg.add(rdbtnDisease);
		bg.add(rdbtnDrug);
		bg.add(rdbtnSymptoms);
		
		
		JButton btnValider = new JButton("Search");
		btnValider.setFont(new Font("MV Boli", Font.BOLD, 17));
		btnValider.setForeground(new Color(51, 204, 0));
		btnValider.setEnabled(true);
		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(rdbtnDisease.isSelected()==true){
					String str = collectName.getText();
					String str1 = textField_Disease.getText();
					if(!str1.equals("")){
					ResultsWindow frame;
					try {
						
						frame = new ResultsWindow(mapping.requestOR(textField_Disease.getText()),null,null, mapping, str, str1, 0);
						frame.setVisible(true);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					dispose();
				}
					else{
						lblRentrezLeHashtag.setVisible(true);
					}
					}
				else if(rdbtnDrug.isSelected()== true){
					String str = collectName.getText();
					String str1 = textField_Drug.getText();
					if(!str1.equals("")){
						
					try {
						Drug d = mapping.requestDrugOR(str1);
						ResultsWindow frame;
						frame = new ResultsWindow(null,d,null, mapping, str, str1,1);
						frame.setVisible(true);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					dispose();
				}
					else{
						lblRentrezLaRfrence.setVisible(true);
					}
				}
				else if(rdbtnSymptoms.isSelected()== true){
					String str = collectName.getText();
					String str1 = textField.getText();
					if(!str1.equals("")){
					ResultsWindow frame;
					try {
						frame = new ResultsWindow(null, null,mapping.requestSymptomsOR(textField.getText()), mapping, str, str1,2);
						frame.setVisible(true);
					} catch (IOException | JSONException | org.apache.lucene.queryparser.classic.ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					dispose();
				}
					else{
						lblRentrezLesSymptoms.setVisible(true);
					}
				}	
					
				
			}
		});
		btnValider.setBounds(312, 522, 128, 48);
		contentPane.add(btnValider);
		
	}
}
