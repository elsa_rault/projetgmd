package ui.graphical;
import java.io.File;
import java.io.IOException;

import javax.swing.filechooser.FileFilter;
 
/**
 * This class is used to display extension in the selectbox which allows to search where the CSV file is saved
 * 
 */

	public class FileExtension extends FileFilter {
	 private String extension ;
	 private String description ;
	 
	 	/**
	  	 * Constructor of the class
		 * 
		 * @param extension
		 * @param description
		 */
	 public FileExtension(final String extension, final String description)
	 {
	  if (extension != null)
	  {
	   this.extension = extension.toLowerCase() ;
	  }
	  
	  this.description = description ;
	 }
	 
	 @Override
	 public boolean accept(File f) {
	  return (testFile(f.getAbsolutePath()) || f.isDirectory()) ;
	 }

	 @Override
	 public String getDescription() {
	  return description ;
	 }

	 /** This method allows to verify if yhe fileExtension is right
		 * 
		 * @param fileName
		 * @return  boolean
		 */
	 private boolean testFile(final String fileName)
	 {
	  return ((extension != null) && (!"".equals(extension)) &&
	    fileName.toLowerCase().endsWith(extension)) ;
	 }
	}
