package Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/** Class for Drug element.
 *  A drug is composed of a name, a list of disease name indications of drug, a list of disease name for side effects, a list of sources and a score
 * @author Elsa
 *
 */
 
public class Drug {

	String name;
	ArrayList<String> indications;
	ArrayList<String> side_effect;
	ArrayList<String> sources;
	int score;

	/** constructeur
	 * 
	 * @param name
	 * @param indications
	 * @param side_effect
	 * @param sources
	 * @param score
	 */
	public Drug (String name, ArrayList<String> indications, ArrayList<String> side_effect, ArrayList<String> sources, int score){
		this.name=name;
		this.indications=indications;
		this.side_effect=side_effect;
		this.sources=sources;
		this.score=score;
	}

	/** This method creates an ArrayList of drug as the result of the intersection of two ArrayList of drug
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static ArrayList<Drug> intersectionDrug( ArrayList<Drug> d1, ArrayList<Drug> d2){
		if (d1==null || d2 ==null){
			return null;
		} 
		ArrayList <Drug> drugList= new ArrayList<Drug>();
		for (int i=0; i<d1.size(); i++){
			for (int j=0; j<d2.size();j++){
				if (d1.get(i).getName().toUpperCase().equals(d2.get(j).getName().toUpperCase())){
					if (d1.get(i).getScore()>=d2.get(j).getScore()){
						drugList.add(d1.get(i));
					}else {
						drugList.add(d2.get(j));
					}
				}
			}
		}
		return drugList;
	}
	
	/** This method creates Drug as the union of two Drugs
	 * 
	 * @param d
	 * @param d1
	 * @return
	 */
	public static Drug union(Drug d, Drug d1){
		ArrayList<String> indications=null;
		ArrayList<String> side_effect=null;
		int score=0;
		ArrayList<String> sources=null;
		
		int modifications1=0;
		int modifications2=0;
		if (d==null){
			if (d1==null){
				return null;
			}
			return d1;
		}else if(d!=null && d1==null) {
			return d;
		}

		if (d.getIndications()==null || d.getIndications().isEmpty()){
			if(d1.indications==null || d1.indications.isEmpty()){
				indications=null;
			}else{
				indications=d1.indications;
				modifications1++;
			}
		}else if(d1.indications==null || d1.indications.isEmpty()){
			indications=d.getIndications();
			modifications2++;
		}else {
			indications=d1.indications;
			modifications1++;
			for (int i=0; i<d.getIndications().size();i++){
				if (!indications.contains(d.getIndications().get(i))){
					indications.add(d.getIndications().get(i));
					modifications2++;
				}
			}
		}
		if (d.getSide_effect()==null || d.getSide_effect().isEmpty()){
			if(d1.side_effect==null || d1.side_effect.isEmpty()){
				side_effect=null;
			}
			else {
				side_effect=d1.side_effect;
				modifications1++;
			}			
		}else if(d1.side_effect==null || d1.side_effect.isEmpty()){
			side_effect=d.getSide_effect();
			modifications2++;
		}else {
			side_effect=d1.side_effect;
			modifications1++;
			for (int i=0; i<d.getSide_effect().size();i++){
				if (!side_effect.contains(d.getSide_effect().get(i))){
					side_effect.add(d.getSide_effect().get(i));
					modifications2++;
				}
			}
		}
		if (d.getName().toUpperCase().equals(d1.getName().toUpperCase())){
			modifications1++;
			modifications2++;
		} else {
			d.setName(d.getName().toUpperCase()+" || "+d1.getName().toUpperCase());
		}
		score=1;
		if (modifications1>0 && modifications2>0){
			sources=d1.getSources();
			for (int i=0; i<d.getSources().size();i++){
				if (!sources.contains(d.getSources().get(i))){
					sources.add(d.getSources().get(i));
					score=d1.getScore()+d.getScore();
				}
			}
		}else if (modifications1>0){
			sources=d1.getSources();
		}else if (modifications2>0){
			sources= d.getSources();
		}
		Drug drugResult= new Drug(d.getName().toUpperCase(),indications,side_effect,sources,score);
		sources=null;
		return drugResult;
	}

	/**This method creates Drug as the intersection of two Drugs
	 * 
	 * @param d
	 * @param d1
	 * @return
	 */
	
	public static Drug intersection(Drug d, Drug d1){
		ArrayList<String> indications=null;
		ArrayList<String> side_effect=null;
		int score=0;
		ArrayList<String> sources=null;
		
		int modifications1=0;
		int modifications2=0;
		if (d==null){
			if (d1==null){
				return null;
			}
			return null;
		}else if(d!=null && d1==null) {
			return null;
		}
		if (d.getIndications()==null || d.getIndications().isEmpty()){
			indications=null;
		}else if(d1.getIndications()==null || d1.getIndications().isEmpty()){
			indications=null;
		}else {
			d.getIndications().retainAll(d1.getIndications());
			indications=d.getIndications();
			modifications1++;
			modifications2++;
		}
		if (d.getSide_effect()==null || d.getSide_effect().isEmpty()){
			side_effect=null;
		}else if(d1.getSide_effect()==null || d1.getSide_effect().isEmpty()){
			side_effect=null;
		}else {
			d1.getSide_effect().retainAll(d.getSide_effect());
			side_effect=d1.getSide_effect();
			modifications1++;
			modifications2++;
		}
		if (d.getName().toUpperCase().equals(d1.getName().toUpperCase())){
			modifications1++;
			modifications2++;
		}else {
			d.setName(d.getName().toUpperCase()+" && "+d1.getName().toUpperCase());
		}
		score=1;
		if (modifications1>0 && modifications2>0){
			sources=d1.getSources();
			for (int i=0; i<d.getSources().size();i++){
				if (!sources.contains(d.getSources().get(i))){
					sources.add(d.getSources().get(i));
					score=d1.getScore()+d.getScore();
				}
			}
		}else if (modifications1>0){
			sources=d1.getSources();
		}else if (modifications2>0){
			sources= d.getSources();
		}
		Drug drugResult= new Drug(d.getName().toUpperCase(),indications,side_effect,sources,score);
		sources=null;
		return drugResult;
		
	}

	/** This method creates ArrayList of Drug as the union of two ArrayList of Drug
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static ArrayList<Drug> unionDrug( ArrayList<Drug> d1, ArrayList<Drug> d2){
		int count=0;
		ArrayList<Drug> drugList= new ArrayList<Drug>();
		if (d1==null){
			if (d2==null){
				return null;
			}
			Disease.setModificationd2(Disease.getModificationd2()+1);
			drugList=d2;
			return drugList;
		}else if (d2==null){
			Disease.setModificationd1(Disease.getModificationd1()+1);
			drugList=d1;
			return drugList;
		}
		drugList=d1;
		int size=d2.size();
		for (int i=0; i<size;i++){
			int modif=0;
			for(int j=0;j<d1.size();j++){
				if(d1.get(j).getName().toUpperCase().equals(d2.get(i).getName().toUpperCase()) && !d2.get(i).getName().isEmpty() ){
					
					if(!d1.get(j).getSources().equals(d2.get(i).getSources())){
						Set<String> set= new HashSet<String>(d1.get(j).getSources());
				        set.addAll(d2.get(i).getSources()) ;
				        ArrayList<String> distinctList = new ArrayList<String>(set) ;
						drugList.get(j).setSources(distinctList);
						drugList.get(j).setScore(drugList.get(j).getSources().size());
					}
					modif=1;
				}
			}
			if (modif==0){
				drugList.add(d2.get(i));
			}
		}
		Disease.setModificationd1(Disease.getModificationd1()+1);
		Disease.setModificationd2(Disease.getModificationd2()+1);
		return d1;
	}
	
	/** This method sorts an ArrayList of Drug according to his score and then his alphabetical order
	 * 
	 * @param drugList
	 * @return
	 */
	public static ArrayList<Drug> sortDrug(ArrayList<Drug> drugList){
		List<Drug> drugs= new ArrayList<Drug>(drugList);
		Collections.sort(drugs,new Comparator<Drug>(){
			public int compare(Drug d1, Drug d2){
				if (d1==null){
					if (d2!=null) return -1;
				}
				if (d2==null) return 1;
				int result= d1.getName().compareTo(d2.getName());
				return result;
			}
		});
		Collections.sort(drugs,new Comparator<Drug>(){
			public int compare(Drug d1, Drug d2){
				if (d1==null){
					if (d2!=null) return -1;
				}
				if (d2==null) return 1;
				int result= Integer.compare(d1.getScore(),d2.getScore());
				return result;
			}
		}.reversed());
		ArrayList<Drug> drug= new ArrayList<Drug>(drugs);
		return drug;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<String> getIndications() {
		return indications;
	}

	public void setIndications(ArrayList<String> indications) {
		this.indications = indications;
	}

	public ArrayList<String> getSide_effect() {
		return side_effect;
	}

	public void setSide_effect(ArrayList<String> side_effect) {
		this.side_effect = side_effect;
	}

	public String toString(){
		String result="name: "+this.getName()+" sources: "+this.getSources()+" score: "+this.getScore();
		return result;
	}
	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public ArrayList<String> getSources() {
		return sources;
	}

	public void setSources(ArrayList<String> sources) {
		this.sources = sources;
	}

}
