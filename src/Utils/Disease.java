package Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/** Class for Disease element.
 * A disease is composed of a name, a list of synonyms, a list of symptoms, a list of Drugs that cure this disease, a list of Drugs that cause this disease, all sources and a score 
 * 
 * @author Elsa and Clementine
 *
 */
public class Disease {

	String name;
	ArrayList<Disease> symptoms;
	ArrayList<Drug> drugs;
	ArrayList<Drug> parentsDrugs;
	ArrayList<String> source;
	int score;
	ArrayList<String> synonyms;
	static int modificationd1;
	static int modificationd2;

	public Disease (String name, ArrayList<String> synonyms,  ArrayList<Disease> symptoms, ArrayList<Drug> drugs, ArrayList<Drug> parentsDrugs, ArrayList<String> source, int score){
		this.name=name;
		this.synonyms=synonyms;
		this.symptoms=symptoms;
		this.drugs= drugs;
		this.parentsDrugs=parentsDrugs;
		this.source=source;
		this.score=score;
	}

	/** This method creates a Disease that is the intersection of two Diseases
	 * 
	 * @param d1: disease 1
	 * @param d2: disease 2
	 * @return Disease 
	 */
	public static Disease intersection(Disease d1, Disease d2){
		String name="";
		ArrayList<String> synonyms= null;
		ArrayList<Disease> symptoms=null;
		ArrayList<Drug> drug=null;
		ArrayList<Drug> drugParent=null;
		ArrayList<String> source=null;

		if (d1==null || d1.isNull() || d2==null || d2.isNull()){
			return null;
		}else {

			name = d1.getName()+" && "+d2.getName();
			if (d1.getSynonyms()!=null && d2.getSynonyms()!=null){
				synonyms=d1.getSynonyms();
				synonyms.retainAll(d2.getSynonyms());
				if (synonyms.isEmpty()){
					synonyms=null;
				}
			}
			if (d1.getSymptoms()!=null && d2.getSymptoms()!=null){
				symptoms=intersectionDisease(d1.getSymptoms(), d2.getSymptoms());
			}
			if (d1.getDrugs()!=null && d2.getDrugs()!=null){
				drug=Drug.intersectionDrug(d1.getDrugs(), d2.getDrugs());
			}
			if (d1.getParentsDrugs()!=null && d2.getParentsDrugs()!=null){
				drugParent=Drug.intersectionDrug(d1.getParentsDrugs(), d2.getParentsDrugs());
			}
			if (d1.getSource()!=null && d2.getSource()!=null){
				source=d1.getSource();
				source.retainAll(d2.getSource());
				if (source.isEmpty()){
					source=null;
				}
			}
			Disease dis= new Disease(name, synonyms,symptoms, drug, drugParent, source, 0);
			return dis;
		}
		
	}

	/** This method creates a Disease that is the union of two Diseases
	 * 
	 * @param d1: Disease 1
	 * @param d2: Disease 2
	 * @return Disease
	 */
	public static Disease union(Disease d1, Disease d2){
	
		ArrayList<String> synonyms= null;
		ArrayList<Disease> symptoms=null;
		ArrayList<Drug> drug=null;
		ArrayList<Drug> drugParent=null;
		ArrayList<String> source=new ArrayList<String>();

		modificationd1=0;
		modificationd2=0;
		if (d1==null || d1.isNull()){
			if (d2==null || d2.isNull()){
				return null;
			}else {
				return d2;
			}
		}else if (d2==null || d2.isNull()){
			return d1;
		}else {
			if (d1.getSynonyms()==null){
				if (d2.getSynonyms()!=null){
					modificationd2++;
					synonyms=d2.getSynonyms();
				}
			} else if(d2.getSynonyms()==null){
				modificationd1++;
				synonyms=d1.getSynonyms();
			} else {

				synonyms=d1.getSynonyms();
				for (int i=0;i<d2.getSynonyms().size();i++){
					if(!synonyms.contains(d2.getSynonyms().get(i)) && !d2.getSynonyms().get(i).trim().isEmpty()){
						synonyms.add(d2.getSynonyms().get(i));
					}
				}
				modificationd2++;
				modificationd1++;
			}
			symptoms=unionDisease(d1.getSymptoms(), d2.getSymptoms());
			drug=Drug.unionDrug( d1.getDrugs(), d2.getDrugs());
			drugParent=Drug.unionDrug( d1.getParentsDrugs(), d2.getParentsDrugs());
			if (modificationd1>0){
				for (int i=0; i<d1.getSource().size(); i++){
					if (!source.contains(d1.getSource().get(i))){
						source.add(d1.getSource().get(i));						
					}
				}	
			}
			if (modificationd2>0){
				for (int i=0; i<d2.getSource().size(); i++){
					if (!source.contains(d2.getSource().get(i))){
						source.add(d2.getSource().get(i));						
					}
				}	
			}
			if (!d1.getName().toLowerCase().contains(d2.getName().toLowerCase())){
				d1.setName(d1.getName()+" || "+d2.getName());
			}
			Disease d= new Disease(d1.getName(),synonyms, symptoms, drug, drugParent, source,0);
			return d;
		}
	}



	/** This method informs if element Disease is empty
	 * 
	 * @return boolean
	 */
	public boolean isNull(){
		if (this.getSymptoms()==null && this.getSynonyms()==null && this.getDrugs()==null && this.getParentsDrugs()==null){
			return true;
		}
		return false;
	}
	
	/** This method creates ArrayList of disease that is the intersection of two arraylist of disease
	 * 
	 * @param d1: arrayList of disease
	 * @param d2: arrayList of disease
	 * @return intersection of two arraylist of disease
	 */
	public static ArrayList<Disease> intersectionDisease( ArrayList<Disease> d1, ArrayList<Disease> d2){
		if (d1== null && d2==null){
			return null;
		}
		if (d1==null && d2!=null){
			modificationd2++;
			return d2;
		}
		if (d2==null && d1!=null){
			modificationd1++;
			return d1;
		}
		d1.retainAll(d2);
		modificationd2++;
		modificationd1++;
		return d1;
	}

	/** This method creates ArrayList of disease that is the union of two arraylist of disease
	 * 
	 * @param d1: arraylist of disease
	 * @param d2: arraylist of disease
	 * @return the union of two arraylist of disease
	 */
	public static ArrayList<Disease> unionDisease( ArrayList<Disease> d1, ArrayList<Disease> d2){
		if (d1== null && d2==null){
			return null;
		}
		if (d1==null && d2!=null){
			modificationd2++;
			return d2;
		}
		if (d2==null && d1!=null){
			modificationd1++;
			return d1;
		}
		for (int i=0; i<d2.size();i++){
			if (!d1.contains(d2.get(i))){
				d1.add(d2.get(i));

			}
		}
		modificationd2++;
		modificationd1++;
		return d1;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static int getModificationd1() {
		return modificationd1;
	}

	public static void setModificationd1(int modificationd1) {
		Disease.modificationd1 = modificationd1;
	}

	public static int getModificationd2() {
		return modificationd2;
	}

	public static void setModificationd2(int modificationd2) {
		Disease.modificationd2 = modificationd2;
	}

	public ArrayList<Disease> getSymptoms() {
		return symptoms;
	}

	public void setSymptoms(ArrayList<Disease> symptoms) {
		this.symptoms = symptoms;
	}

	public ArrayList<Drug> getDrugs() {
		return drugs;
	}

	public void setDrugs(ArrayList<Drug> drugs) {
		this.drugs = drugs;
	}

	public ArrayList<Drug> getParentsDrugs() {
		return parentsDrugs;
	}

	public void setParentsDrugs(ArrayList<Drug> parentsDrugs) {
		this.parentsDrugs = parentsDrugs;
	}

	public ArrayList<String> getSource() {
		return source;
	}

	public void setSource(ArrayList<String> source) {
		this.source = source;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String toString(){
		String result;
		result= "name: "+name+ " | synonyms: "+synonyms+" | symptoms:  "+ symptoms+" | drugs: "+drugs+" | parent drugs: "+parentsDrugs+" | sources: "+source+" | score: "+score;
		return result;
	}

	@SuppressWarnings("unused")
	public void print(){
		if (this==null){
			System.out.println("==================Results================");
			System.out.println("No results");
		}else {
			System.out.println("==================Results================");
			System.out.println("");
			System.out.println("name: "+this.getName());
			System.out.println("");
			System.out.println("******************SYNONYMS***************");
			System.out.println("");
			if (this.getSynonyms()==null){
				System.out.println("no synonyms");
			} else {
				for (int i=0; i<this.getSynonyms().size();i++){
					if (this.getSynonyms().get(i)!=null && !this.getSynonyms().get(i).trim().isEmpty()){
						System.out.println(i+": "+this.getSynonyms().get(i).trim());
					}
				}
			}
			System.out.println("");
			System.out.println("*****************SYMPTOMS*****************");
			System.out.println("");
			if (this.getSymptoms()==null){
				System.out.println("no symptoms");
			} else {
				for (int i=0; i<this.getSymptoms().size();i++){
					if (this.getSymptoms().get(i)!=null && !this.getSymptoms().get(i).getName().trim().isEmpty()){
						System.out.println(i+": "+this.getSymptoms().get(i).getName().trim());
					}
				}
			}
			System.out.println("");
			System.out.println("**************DRUG INDICATION**************");
			System.out.println("");
			if (this.getDrugs()==null){
				System.out.println("no drug indication");
			} else {
				this.setDrugs(Drug.sortDrug(this.getDrugs()));
				for (int i=0; i<this.getDrugs().size();i++){
					if (this.getDrugs().get(i).getName()!=null && !this.getDrugs().get(i).getName().trim().isEmpty()){
						System.out.println(i+": "+this.getDrugs().get(i).getName().trim().toUpperCase()+" ; score: "+this.getDrugs().get(i).getScore()+" ; sources: "+this.getDrugs().get(i).getSources());
					}
				}
			}
			System.out.println("");
			System.out.println("**************DRUG CAUSE DISEASE***********");
			System.out.println("");
			if (this.getParentsDrugs()==null){
				System.out.println("no drug cause disease");
			} else {
				this.setParentsDrugs(Drug.sortDrug(this.getParentsDrugs()));
				for (int i=0; i<this.getParentsDrugs().size();i++){
					if (this.getParentsDrugs().get(i).getName()!=null && !this.getParentsDrugs().get(i).getName().isEmpty()){
						System.out.println(i+": "+this.getParentsDrugs().get(i).getName().trim()+" ; score: "+this.getParentsDrugs().get(i).getScore()+" ; sources: "+this.getParentsDrugs().get(i).getSources());
					}
				}
			}
			System.out.println("");
			System.out.println("******************SOURCES*******************");
			System.out.println("");
			if (this.getSource()==null){
				System.out.println("no source");
			} else {
				for (int i=0; i<this.getSource().size();i++){
					System.out.println(i+": "+this.getSource().get(i).trim());
				}
			}
			System.out.println("");
		}
	}
	public static ArrayList<Drug> sortDrug(ArrayList<Drug> drugList){
		List<Drug> drugs= new ArrayList<Drug>(drugList);
		Collections.sort(drugs,new Comparator<Drug>(){
			public int compare(Drug d1, Drug d2){
				if (d1==null){
					if (d2!=null) return -1;
				}
				if (d2==null) return 1;
				int result= d1.getName().compareTo(d2.getName());
				return result;
			}
		});
		Collections.sort(drugs,new Comparator<Drug>(){
			public int compare(Drug d1, Drug d2){
				if (d1==null){
					if (d2!=null) return -1;
				}
				if (d2==null) return 1;
				int result= Integer.compare(d1.getScore(),d2.getScore());
				return result;
			}
		}.reversed());
		ArrayList<Drug> drug= new ArrayList<Drug>(drugs);
		return drug;
	}
	
	public static ArrayList<Disease> sortDisease(ArrayList<Disease> diseaseList){
		List<Disease> disease= new ArrayList<Disease>(diseaseList);
		Collections.sort(disease,new Comparator<Disease>(){
			public int compare(Disease d1, Disease d2){
				if (d1==null){
					if (d2!=null) return -1;
				}
				if (d2==null) return 1;
				int result= d1.getName().compareTo(d2.getName());
				return result;
			}
		});
		ArrayList<Disease> diseases= new ArrayList<Disease>(disease);
		return diseases;
	}
	public ArrayList<String> getSynonyms() {
		return synonyms;
	}

	public void setSynonyms(ArrayList<String> synonyms) {
		this.synonyms = synonyms;
	}

}
