package slider;

/**This class creates connection with Slider 2, a mysql database
 * 
 * @author Elsa and Clementine
 * 
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.sun.xml.internal.ws.api.message.Message;
import com.sun.xml.internal.ws.api.message.Messages;

import Utils.Disease;
import Utils.Drug;

public class SQLRequest {
	
	// all informations for the connection to mysql database
	private final static String DB_SERVER = "jdbc:mysql://neptune.telecomnancy.univ-lorraine.fr:3306/";
	private final static String DB = "gmd";
	private final static String DRIVER = "com.mysql.jdbc.Driver";
	private final static String USER_NAME = "gmd-read";
	private final static String USER_PSWD = "esial";
	
	// all initialisation of global variables 
	private Connection connection=null;
	private PreparedStatement Names=null;
	private PreparedStatement Drug1=null;
	private PreparedStatement Drug2=null;
	private PreparedStatement DiseaseCausedBy=null;
	private PreparedStatement SideEffects=null;
	private PreparedStatement IndicationForDrug=null;
	private PreparedStatement Parents=null;
	private ResultSet resultat=null;
	
	private ArrayList<String> indications=new ArrayList<String>();
	private ArrayList<String> drugCauseDisease= new ArrayList<String>();
	private ArrayList<String> diseasesName= new ArrayList<String>(); 
	
	private String name;

	//Request that allows to find all drugs by disease
	private String queryDiseasesName = "SELECT DISTINCT se_name "
			+ "FROM adverse_effects_raw "
			+ "WHERE se_name LIKE ?";
	
	//Resquest that allows to find all indications by drug where drug_name1  is not empty
	private String queryDrug1 = "SELECT drug_name1, i_name as name "
			+ "FROM indications_raw ,label_mapping "
			+ "WHERE indications_raw.label=label_mapping.label "
			+ "AND i_name LIKE ?"
			+ "AND drug_name1 != ''";
	
	
	//Resquest that allows to find all drugs by indications informations where drug_name1 is empty and marker is different from combinaison, not found, mapping_conflict or template
	private String queryDrug2 = "SELECT drug_name2, i_name as name, drug_name1, marker "
			+ "FROM indications_raw ,label_mapping "
			+ "WHERE indications_raw.label=label_mapping.label "
			+ "AND i_name LIKE ?"
			+ "AND drug_name1 = ''"
			+ "AND marker != 'combination'"
			+ "AND marker != 'not found'"
			+ "AND marker != 'mapping_conflict'"
			+ "AND marker != 'template'";
	
	//Request that allows to find all  indications for a drug 
	private String queryIndicationForDrug = "SELECT i_name as name, drug_name1 "
			+ "FROM indications_raw ,label_mapping "
			+ "WHERE indications_raw.label=label_mapping.label "
			+ "AND (drug_name1=? OR drug_name2=?) "
			+ "AND marker != 'combination' "
			+ "AND marker != 'not found' "
			+ "AND marker != 'mapping_conflict' "
			+ "AND marker != 'template'";
	
	//Request that allows to find all drugs by a side effect
	private String queryDiseaseCausedBy = "SELECT drug_name1,drug_name2,se_name as name "
			+ "FROM adverse_effects_raw ,label_mapping "
			+ "WHERE adverse_effects_raw.label=label_mapping.label "
			+ "AND se_name LIKE ?"
			+ "AND marker != 'combination'"
			+ "AND marker != 'not found'"
			+ "AND marker != 'mapping_conflict'"
			+ "AND marker != 'template'";

	
	//Request that allows to find side effects by drug
	private String querySideEffects = "SELECT drug_name1 ,drug_name2 ,se_name as name "
			+ "FROM adverse_effects_raw ,label_mapping "
			+ "WHERE adverse_effects_raw.label=label_mapping.label "
			+ "AND (drug_name1=? OR drug_name2=?)";
	
	
	/**Constructor
	 * Establish the connection to sql
	 */
	public SQLRequest(){
		
		try {
			System.out.println(("Loading driver..."));
			Class.forName(DRIVER);
			System.out.println("Driver loaded!");
		} catch (ClassNotFoundException e){
			System.out.println("Error loading driver: "+e.getMessage());
		}
		
		try {
			System.out.println("Connection to database...");
			connection = DriverManager.getConnection(DB_SERVER + DB, USER_NAME,
					USER_PSWD);
			System.out.println("Successful connection");
			
		}catch (SQLException e){
			System.out.println("Error during connection: "+e.getMessage());
		}
	}
	
	/** This method allows to recover all informations about a drug by his name
	 * 
	 * @param drug: name of the drug you search
	 * @return Drug
	 */
	public Drug SQLRequest2(String drug) throws SQLException{
		indications=new ArrayList<String>();
		drugCauseDisease= new ArrayList<String>();
		ArrayList<String> indications= getIndicationForDrug(drug);
		ArrayList<String> sideEffect= getSideEffectForDrug(drug);
		ArrayList<String> sources = new ArrayList<String>();
		sources.add("Sider 2");
		if (!sideEffect.isEmpty() || !indications.isEmpty()){
			Drug d = new Drug(drug.toUpperCase(),indications,sideEffect,sources,1);
			return d;
		}
		return null;
	}
	
	
	/** This method allows to find all diseases names by disease name containing wildcards
	 * 
	 * @param disease: name with wildcards
	 * @return  list of disease name 
	 */
	public ArrayList<String> getDiseasesName(String disease) throws SQLException{
			
			disease=disease.replaceAll("\\?","_");
			disease=disease.replaceAll("\\*","%");
		
			diseasesName=new ArrayList<String>();
			Names= connection.prepareStatement(queryDiseasesName);
			Names.setString(1, disease);
			resultat= Names.executeQuery();
			
			while (resultat.next()){
				diseasesName.add(resultat.getString("se_name").toUpperCase());
			}
			return diseasesName;
	}
	
	
	/** This method recovers all drugs that cure this disease and all drugs that cause this disease
	 * 
	 * @param disease: name of the disease
	 */
	public void SQLRequest1(String disease) throws SQLException{
			
			disease=disease.replaceAll("\\?","_");
			disease=disease.replaceAll("\\*","%");
					
			indications=new ArrayList<String>();
			drugCauseDisease= new ArrayList<String>();
			
			//Recover all drugs that cure this disease 
			Drug1 =connection.prepareStatement(queryDrug1);
			Drug1.setString(1,disease);
			resultat= Drug1.executeQuery();
			while (resultat.next()){
				if (!indications.contains(resultat.getString("drug_name1").toUpperCase())){
					indications.add(resultat.getString("drug_name1").toUpperCase());
					setName(resultat.getString("name"));
				}
			}
			Drug2 =connection.prepareStatement(queryDrug2);
			Drug2.setString(1,disease);
			resultat= Drug2.executeQuery();
			while (resultat.next()){
				if (!indications.contains(resultat.getString("drug_name2").toUpperCase())){
					indications.add(resultat.getString("drug_name2").toUpperCase());
				}
			}

			// recover all drugs that cause this disease
			DiseaseCausedBy= connection.prepareStatement(queryDiseaseCausedBy);
			DiseaseCausedBy.setString(1,disease);
			resultat= DiseaseCausedBy.executeQuery();
			while (resultat.next()){
				if (resultat.getString("drug_name1")==""){
					if (!drugCauseDisease.contains(resultat.getString("drug_name2").toUpperCase())){
						drugCauseDisease.add(resultat.getString("drug_name2").toUpperCase());
					}
				}else{
					if (!drugCauseDisease.contains(resultat.getString("drug_name1").toUpperCase())){
						drugCauseDisease.add(resultat.getString("drug_name1").toUpperCase());
					}
				}
			}
	}
			
	/** This method recovers all diseases that the drug cures 
	 * 
	 * @param drug: name of the drug
	 * @return  list of diseases names
	 */
	public ArrayList<String> getIndicationForDrug(String drug) throws SQLException{
		ArrayList<String> indications= new ArrayList<String>();
		IndicationForDrug= connection.prepareStatement(queryIndicationForDrug);
		IndicationForDrug.setString(1,drug);
		IndicationForDrug.setString(2,drug);
		resultat= IndicationForDrug.executeQuery();
		while (resultat.next()){
			if (!resultat.getString("name").isEmpty() && !resultat.getString("name").equals("")){
				if (!indications.contains(resultat.getString("name").toUpperCase())){
					indications.add(resultat.getString("name").toUpperCase());
				}
			}
		}
		
		
		return indications;
	}
	
	/**This method creates the disease
	 * 
	 * @param disease: name of the disease
	 * @return  list of disease
	 */
	public ArrayList<Disease> setDisease(String disease) throws SQLException{
		ArrayList<String> source= new ArrayList<String>();
		ArrayList<Disease> diseasesList = new ArrayList<Disease>();
		source.add("Slider 2");
		diseasesName=this.getDiseasesName(disease);
		if (diseasesName.isEmpty()){
			System.out.println("no result in mysql database for "+disease);
			return null;
		}
		for (int i=0;i<diseasesName.size();i++){
			this.SQLRequest1(diseasesName.get(i));
			Disease d= new Disease(diseasesName.get(i).toUpperCase(),null,null,setDrugs(indications),setDrugs(drugCauseDisease),source,0);
			if (!diseasesList.contains(d) && d.getName()!=null && !d.getName().isEmpty()){
				diseasesList.add(d);
			}
				
		}
		
		return diseasesList;
	}
	
	
	/** This method recovers all diseases that the drug causes
	 * 
	 * @param drug: name of the drug
	 * @return list of disease name
	 */
	public ArrayList<String> getSideEffectForDrug(String drug) throws SQLException{
		ArrayList<String> sideEffect= new ArrayList<String>();
		SideEffects= connection.prepareStatement(querySideEffects);
		SideEffects.setString(1,drug);
		SideEffects.setString(2,drug);
		resultat= SideEffects.executeQuery();
		while (resultat.next()){
			if (!resultat.getString("name").isEmpty() && !resultat.getString("name").equals("")){
				if (!sideEffect.contains(resultat.getString("name").toUpperCase())){
					sideEffect.add(resultat.getString("name").toUpperCase());
				}
			}
		}
		
		
		return sideEffect;
	}
	
	
	/** This method creates all drugs with all drugs names
	 * 
	 * @param drugs; list of drugs names
	 * @return list of Drug element
	 */
	public ArrayList<Drug> setDrugs(ArrayList<String> drugs) throws SQLException{
		ArrayList<Drug> drugList= new ArrayList<Drug>();
		ArrayList<String> sources = new ArrayList<String>();
		sources.add("Sider 2");
		for (int i=0;i<drugs.size(); i++){
			Drug drug =new Drug(drugs.get(i).toUpperCase(),null,null,sources,1);
			if (!drug.getName().isEmpty()){
				drugList.add(drug);
			}
		}
		if (drugList.isEmpty()){
			return null;
		}
		return drugList;
	}
	
	/** This method closes all preparedStatements
	 * 
	 */
	public void close() throws Exception {
		if (DiseaseCausedBy!=null){
			DiseaseCausedBy.close();
		}
		if (Drug1!=null){
			Drug1.close();
		}
		if (Drug2!=null){
			Drug2.close();
		}
		if (SideEffects!=null){
			SideEffects.close();
		}
		if (connection!=null){
			connection.close();
		}
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
