package slider;

import java.util.ArrayList;

import Utils.Disease;
import Utils.Drug;

/** Class for testing connection with Sider database and some request
 * 
 * @author Elsa and Clementine
 *
 */
public class TestSQLRequest {
	
	
	public static void main(String[] args) throws Exception {
		SQLRequest sql= new SQLRequest();
		ArrayList<Disease> diseases= sql.setDisease("throat*");
		if (diseases!=null){
			for (int i=0; i<diseases.size(); i++){
				System.out.println("*****************************");
				if (diseases.get(i).getDrugs()!=null){
					for (int j=0; j<diseases.get(i).getDrugs().size(); j++){
						System.out.println(j+": "+diseases.get(i).getDrugs().get(j));
						
					}
				}else{
					System.out.println("no drugs indications");
				}
				System.out.println("*****************************");
				if (diseases.get(i).getParentsDrugs()!=null){
					for (int j=0; j<diseases.get(i).getParentsDrugs().size(); j++){
						System.out.println(j+": "+diseases.get(i).getParentsDrugs().get(j));
					}
				}else{
					System.out.println("no parents drug");
				}
				
			}
		}else {
			System.out.println("no results in SQL");
		}
		
		sql.close();
	}

}
