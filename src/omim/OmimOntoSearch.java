package omim;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.RegexpQuery;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import Utils.Disease;
import Utils.Drug;

/** class for searching into index csv or index txt
 * 
 * @author Elsa and Clementine
 *
 */
public class OmimOntoSearch {

	/**this method is used to search disease into file omim
	 * 
	 * @param index: directory where index is stored
	 * @param field: field on which you want to search
	 * @param queryString: request we want 
	 * @return a list of diseases corresponding to the query 
	 * @throws Exception
	 */
	public ArrayList<Disease> Search(String index, String field, String queryString) throws Exception {

		int repeat = 0;
		boolean raw = false;
		String queries = null;
		int hitsPerPage = 1000;

		//creation of the reader
		IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(index)));
		IndexSearcher searcher = new IndexSearcher(reader);
		Analyzer analyzer = new StandardAnalyzer();

		BufferedReader in = null;
		in = new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8));
		QueryParser parser = new QueryParser(field, analyzer);

		while (true) {
			if (queries == null && queryString == null) {                        
				System.out.println("Enter query: ");
			}

			String line = queryString != null ? queryString : in.readLine();

			if (line == null || line.length() == -1) {
				break;
			}

			line = line.trim();
			if (line.length() == 0) {
				break;
			}

			String s=parser.parse(line).toString(field).trim().replace(" ","_");
			Query query = new WildcardQuery(new Term(field,s));
			System.out.println("Searching for: " + s);

			if (repeat > 0) {                           
				Date start = new Date();
				for (int i = 0; i < repeat; i++) {
					searcher.search(query, 100);
				}
				Date end = new Date();
				System.out.println("Time: "+(end.getTime()-start.getTime())+"ms");
			}

			ArrayList<Disease> d=doPagingSearch(in, searcher, query, hitsPerPage, raw, queries == null && queryString == null, index);


			return d;
		}
		reader.close();

		return null;
	}

	/** this method is a paging search scenario, where the search engine presents 
	 * pages of size n to the user. The user can then go to the next page if interested in
	 * the next hits.
	 * 
	 * @param in: it's a BuffererdReader
	 * @param searcher: it's an IndexSearcher 
	 * @param query: request we want 
	 * @param hitsPerPage: 
	 * @param raw
	 * @param interactive
	 * @param index: directory where index is stored
	 * @return  a list of diseases corresponding to the query 
	 */
	public ArrayList<Disease> doPagingSearch(BufferedReader in, IndexSearcher searcher, Query query, 
			int hitsPerPage, boolean raw, boolean interactive,  String index) throws IOException {

		// Collect enough docs to show 5 pages
		TopDocs results = searcher.search(query, 5 * hitsPerPage);
		ScoreDoc[] hits = results.scoreDocs;

		int numTotalHits = results.totalHits;
		System.out.println(numTotalHits + " total matching documents");

		int start = 0;
		int end = Math.min(numTotalHits, hitsPerPage);
		ArrayList<Disease> diseaseList= new ArrayList<Disease>();
		int count=0;

		while (true) {
			if (end > hits.length) {
				System.out.println("Only results 1 - " + hits.length +" of " + numTotalHits + " total matching documents collected.");
				System.out.println("Collect more (y/n) ?");
				String line = in.readLine();
				if (line.length() == 0 || line.charAt(0) == 'n') {
					break;
				}

				hits = searcher.search(query, numTotalHits).scoreDocs;
			}

			end = Math.min(hits.length, start + hitsPerPage);

			for (int i = start; i < end; i++) {
				if (raw) {                              
					// output raw format
					System.out.println("doc="+hits[i].doc+" score="+hits[i].score);
					continue;
				}

				Document doc = searcher.doc(hits[i].doc);
				
				//for search into txt omim source
				if (index=="omim_index"){
					
					//recover name and remove "_"
					String name = doc.get("Name").replaceAll("_"," ");
					
					//recover synonyms by splitting with ";"
					String synonym= doc.get("Synonyms").replaceAll("_"," ");
					ArrayList<String> synonyms= new ArrayList<String>();
					if (!synonym.equals("")){
						String fields[]=synonym.split(";");
						for ( int j=0; j<fields.length; j++){
							synonyms.add(fields[j].toUpperCase());
						}
					}else{
						synonyms=null;
					}
					
					//recover symptoms by splitting with ";"
					String symptoms= doc.get("Symptoms");
					ArrayList<Disease> diseaseSymptoms= new ArrayList<Disease>();
					if (symptoms!=null){
						String field[]=symptoms.split(";");

						ArrayList<String> allSymptoms= new ArrayList<String>();

						for (int j=0; j<field.length; j++){
							allSymptoms.add(field[j].toUpperCase());
						}

						for (int a=0; a<allSymptoms.size(); a++){
							Disease d= new Disease(allSymptoms.get(a).toUpperCase(),null,null,null,null,null, 0);
							diseaseSymptoms.add(d);
						}
					}

					//put the source
					ArrayList<String> source= new ArrayList<String>();
					source.add("Omim");
					
					// create disease and add it to the disease list
					Disease d= new Disease(name.toUpperCase(),synonyms,diseaseSymptoms,null,null,source,0);
					diseaseList.add(d);
					

				}else { // for search into csv omim source
					
					//recover classID
					String classID=doc.get("classID");
					
					
					//recover name and remove "_"
					String name=doc.get("diseaseName").replaceAll("_"," ");
					
					//recover synonyms  and remove "_"
					String synonyms= doc.get("synonymsDiseaseName").replaceAll("_"," ");
					ArrayList<String> synonymsDiseaseName= new ArrayList<String>();
					if (synonyms!=null && !synonyms.equals("  ")){
						String fields[]=synonyms.split("\\|");        		
						for ( int j=0; j<fields.length; j++){
							synonymsDiseaseName.add(fields[j].toUpperCase());
						}
					}else{
						synonymsDiseaseName=null;
					}
					
					//recover classOf
					String classOf= doc.get("classOf");
					ArrayList<String> parentsName= new ArrayList<String>();
					if (classOf!=null){
						String fields[]=classOf.split("\\|");
						for ( int j=0; j<fields.length; j++){
							parentsName.add(fields[j]);
						}
					}
					
					//put the source
					ArrayList<String> source= new ArrayList<String>();
					source.add("Omim");
					
					//create disease and add it to the disease list
					Disease d= new Disease(name.toUpperCase(),synonymsDiseaseName,null,null,null,source,0);
					diseaseList.add(d);
				}



			}

			if (!interactive || end == 0) {
				break;
			}

			if (numTotalHits >= end) {
				boolean quit = false;
				while (true) {
					System.out.print("Press ");
					if (start - hitsPerPage >= 0) {
						System.out.print("(p)revious page, ");  
					}
					if (start + hitsPerPage < numTotalHits) {
						System.out.print("(n)ext page, ");
					}
					System.out.println("(q)uit or enter number to jump to a page.");

					String line = in.readLine();
					if (line.length() == 0 || line.charAt(0)=='q') {
						quit = true;
						break;
					}
					if (line.charAt(0) == 'p') {
						start = Math.max(0, start - hitsPerPage);
						break;
					} else if (line.charAt(0) == 'n') {
						if (start + hitsPerPage < numTotalHits) {
							start+=hitsPerPage;
						}
						break;
					} else {
						int page = Integer.parseInt(line);
						if ((page - 1) * hitsPerPage < numTotalHits) {
							start = (page - 1) * hitsPerPage;
							break;
						} else {
							System.out.println("No such page");
						}
					}
				}
				if (quit) break;
				end = Math.min(numTotalHits, start + hitsPerPage);
			}
		}
		return diseaseList;
	}

	/**this method is used to search symptoms into file omim
	 * 
	 * @param index: directory where index is stored
	 * @param symptom: symptom we have and we search 
	 * @param field: field on which you want to search
	 * @return a list of name of diseases who have this symptom
	 */
	public ArrayList<String> searchSymptoms(String index,String symptom,String field) throws IOException, ParseException{
		
		//create the bufferedReader
		ArrayList<String> diseaseList=null;
		IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(index)));
		IndexSearcher searcher = new IndexSearcher(reader);
		Analyzer analyzer = new StandardAnalyzer();
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8));
		
		//create the query 
		String terms[]=symptom.split(" ");
		BooleanQuery query= new BooleanQuery();
		for (int i=0; i<terms.length;i++){
			WildcardQuery searchQuery= new WildcardQuery(new Term(field,terms[i]));
			query.add(new BooleanClause(searchQuery, Occur.MUST));
		}
		//System.out.println("query: " +query);

		QueryParser parser = new QueryParser(field,analyzer);
		parser.setDefaultOperator(QueryParser.Operator.AND);

		Query query1 = parser.parse(symptom);

		
		
		//called doSymptomSearch for search diseases with this symptom
		diseaseList=doSymptomSearch(searcher, query1, field);
		
		//close the bufferedReader
		reader.close();
		
		
		return diseaseList;
		

	}

	/** this method
	 * 
	 * @param searcher: it's an IndexSearcher
	 * @param query: request we want
	 * @param fields: field on which you want to search
	 * @return  a list of name of diseases who have this symptom
	 */
	public ArrayList<String> doSymptomSearch(IndexSearcher searcher, Query query, String fields) throws IOException {
		
		ArrayList<String> diseaseList= new ArrayList<String>();
		
		// Find the number of hits
		TopDocs result = searcher.search(query, 1);
		ScoreDoc[] hits = result.scoreDocs;
		int numTotalHits = result.totalHits;

		// search only if we have hits
		if (numTotalHits != 0) {
			hits = searcher.search(query, numTotalHits).scoreDocs;

			for (int i = 0; i < numTotalHits; i++) {

				Document doc = searcher.doc(hits[i].doc);

				String toAdd = "";
				toAdd = doc.get("Name");

				if(toAdd!=null && toAdd.length() != 0){
					//find name disease and replace "_"
					diseaseList.add(toAdd.toUpperCase().trim().replace("_"," "));
				}
			}
		}
		return diseaseList;
	}


}
