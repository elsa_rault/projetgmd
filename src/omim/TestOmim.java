package omim;

import java.util.ArrayList;
import Utils.Disease;

/** Class for testing index and search class of package OMIM 
 * 
 * @author Elsa and Clementine 
 *
 */

public abstract class TestOmim {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		OmimOntoSearch search= new OmimOntoSearch();
		//searchSymptoms(String index,String symptom,String field)
		ArrayList<String> disease=search.searchSymptoms("omim_index","c","Symptoms");
		System.out.println("d: "+disease);

	}

}