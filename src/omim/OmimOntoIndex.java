package omim;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

import org.apache.lucene.document.StoredField;

import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

/**this class indexes csv and txt sources with lucene
 * 
 * @author Elsa and Clementine
 *
 */

public class OmimOntoIndex {

	public static void main(String[] args) throws Exception{
		indexOMIM("omim.txt", false);
	}
	

	
		public static final String INDEX_PATH = "omim_index";
		public static final String INDEX_PATH_CSV = "omim_index_csv";

		/** Index all text files and CSV files under a directory. 
		 * 
		 * @param fileToIndex
		 * @param isCSV
		 * @throws Exception
		 */
		public static void indexOMIM(String fileToIndex,
				boolean isCSV) throws Exception {
			Date start = new Date();
			
			File INDEX_DIR = new File(isCSV ? INDEX_PATH_CSV : INDEX_PATH);
			boolean create = true;
			File file = new File(fileToIndex);
			//System.out.println("Indexing to directory '" + isCSV != null ? INDEX_PATH_CSV : INDEX_PATH + "'...");
			Directory directory = FSDirectory.open(Paths.get(isCSV ? INDEX_PATH_CSV : INDEX_PATH));
			Analyzer analyzer = new StandardAnalyzer();
			IndexWriterConfig config = new IndexWriterConfig(analyzer);
			if (create) {
				// Create a new index in the directory, removing any
				config.setOpenMode(OpenMode.CREATE);
			} else {
				config.setOpenMode(OpenMode.CREATE_OR_APPEND);
			}
			IndexWriter writer = new IndexWriter(directory, config);
			if (isCSV) {
				indexCSV(writer, file);
				//System.out.println("csv");
			} else {
				indexDoc(writer, file);
				//System.out.println("txt");
			}
			writer.close();
			
			Date end = new Date();
		    System.out.println(end.getTime() - start.getTime() + " total milliseconds");
		}

		
		/** index for csv file
		 * 
		 * @param writer
		 * @param file
		 * @throws IOException
		 */
		private static void indexCSV(IndexWriter writer, File file)
				throws IOException {

			if (file.canRead() && !file.isDirectory()) {
				// each line of the file is a new document
					InputStream ips = new FileInputStream(file);
					InputStreamReader ipsr = new InputStreamReader(ips);
					BufferedReader br = new BufferedReader(ipsr);
					String line;
					// initialization
					String classID="";
					StringBuffer diseaseName = new StringBuffer("");
					String classOf= "";
					String synonymsDiseaseName ="";
					
					// Parsing and indexing the file
					br.readLine();
					while ((line = br.readLine()) != null) {
						// Split fields
						line=line.replaceAll(";"," ; ");
						//System.out.println(line);

						String fields[]=line.split(";");
						//System.out.println(fields.length);
						classID =fields[0];
						String allTitle=fields[1];
						//for each title, we take off all special character
						allTitle=allTitle.replaceAll(",","");
						allTitle= allTitle.trim();						
						allTitle= allTitle.replaceAll("-"," ");
						allTitle=allTitle.replaceAll("/"," ");
						allTitle=allTitle.toUpperCase();
						//We replace all space by underscore for search exact word
						allTitle= allTitle.replaceAll(" ","_");
						diseaseName.append(allTitle);
						//System.out.println(diseaseName);
						synonymsDiseaseName=fields[2];						
						classOf=fields[7];
						
						// Create a document for each line
						Document doc = new Document();
						doc.add(new TextField("diseaseName", diseaseName.toString(), Field.Store.YES));
						// indexed and stored
						doc.add(new TextField("synonymsDiseaseName", synonymsDiseaseName, Field.Store.YES));
						// indexed and stored
						doc.add(new TextField("classID", classID, Field.Store.YES));
						// indexed and stored
						doc.add(new TextField("classOf", classOf, Field.Store.YES));
						// indexed and stored

						if (writer.getConfig().getOpenMode() == OpenMode.CREATE) {
							writer.addDocument(doc);
						} else {
							System.out.println("updating " + file);
							writer.updateDocument(new Term("path", file.getPath()),
									doc);
						}

						// clean values
						diseaseName = new StringBuffer("");
						synonymsDiseaseName = "";
						classID="";
						classOf="";

					}
					
					br.close();

			}
		}
		
		/** index for omim.txt
		 * 
		 * @param writer
		 * @param file
		 * @throws IOException
		 */
		private static void indexDoc(IndexWriter writer, File file)
				throws IOException {
			int count=0;
			if (file.canRead() && !file.isDirectory()) {
				
					// each line of the file is a new document
					InputStream ips = new FileInputStream(file);
					InputStreamReader ipsr = new InputStreamReader(ips);
					BufferedReader br = new BufferedReader(ipsr);
					String line;
					// initialization
					String id = "";
					String name = "";
					String synonyms="";
					StringBuffer symptoms = new StringBuffer("");
					boolean start = true;
					Document doc = new Document();
					while ((line = br.readLine()) != null) {
						if (line.equals("*RECORD*")) {
							if (start) {
								doc = new Document();
								br.readLine();
								id = br.readLine();
								//System.out.println("adding " + id);
								doc.add(new StoredField("id", id));
								start = false;
							} else {
								writer.addDocument(doc);
								doc = new Document();
								br.readLine();
								id = br.readLine();;
								//System.out.println("adding " + id);
								doc.add(new StoredField("id", id));
							}
						}
						String allTitle="";
						if (line.equals("*FIELD* TI") ) {
							while ((line = br.readLine()) != null) {
								if (line.startsWith("*FIELD*") ) {
									break;
								} else {
									allTitle=allTitle+line;
								}
							}
							if (allTitle.charAt(0)=='*' || allTitle.charAt(0)=='+'){
								
							}
							else{

								//for each title, we take off all special character
								allTitle=allTitle.replaceAll("%","");
								allTitle=allTitle.replaceAll("\\^","");
								allTitle=allTitle.replaceAll("#","");
								allTitle=allTitle.replaceAll("\\-"," ");
								allTitle=allTitle.replaceAll("/"," ");
								allTitle=allTitle.substring(7);
								allTitle=allTitle.replaceAll(";;",";");
								allTitle=allTitle.replaceAll("; ",";");
								allTitle=allTitle.replaceAll(" ;",";");
								allTitle=allTitle.replaceAll(",","");
								allTitle= allTitle.trim();
								//We replace all space by underscore for search exact word
								allTitle=allTitle.replaceAll(" ","_");
								allTitle=allTitle.toUpperCase();
								String fields[]=allTitle.split(";");
								name = fields[0];
								for (int i=1; i<fields.length;i++){
									synonyms=synonyms+fields[i]+"; ";
								}
								//System.out.println("name: "+name.toString());
								//System.out.println("synonyms: "+synonyms);
								doc.add(new TextField("Name", name, Field.Store.YES)); 
								// Indexed and stored
								doc.add(new TextField("Synonyms", synonyms, Field.Store.YES)); 
								// Indexed and stored

								count++;
								//System.out.println(count);
								
							}
								
						}
						
						if (line.equals("*FIELD* CS")) {
							while ((line = br.readLine()) != null) {
								
								if (line.startsWith("*FIELD*")) {
									break;
								} else {
									if (line.equals("")){
										symptoms.append(" # ");
									}else{
										symptoms.append(line).append(" ");
									}

								}
							}
							//System.out.println("Symptoms: " +symptoms);
							String sym=symptoms.toString();
							String allSymptoms="";
							//recovery symptoms
							if (sym!=null){
				            	String field[]=sym.split("#");
				            	ArrayList<String[]> field1= new ArrayList<String[]>();
				                ArrayList<String[]> field2= new ArrayList<String[]>();
				                
				                
				                for (int j=0; j<field.length; j++){
				                	field1.add(field[j].split(":"));
				                	for (int k=0; k<field1.size();k++){
				                		for (int h=1; h<field1.get(k).length;h++){
				                    		field2.add(field1.get(k)[h].split(";"));
				                    		for (int y=0; y<field2.size();y++){
				                            	for (int z=0; z<field2.get(y).length; z++){
				                            		if(!field2.get(y)[z].toString().contains("[") && !allSymptoms.contains(field2.get(y)[z].toString().replaceAll(",",""))){
				                            			String s=field2.get(y)[z].toString().replaceAll(",","");
				                            			allSymptoms+=s+";";
				                            		}
				                            	}
				                            }
				                		}
				                	}
				                }
							}
							allSymptoms=allSymptoms.replaceAll("\\(*\\)", "");
							System.out.println("Sym: " +allSymptoms);
							doc.add(new TextField("Symptoms", allSymptoms, Field.Store.YES));

						}
						id="";
						name = "";
						symptoms = new StringBuffer("");
						synonyms="";
					}
					//adding the last record of OMIM
					writer.addDocument(doc);

					br.close();
			}else{
				System.out.println(file+" is not a file");
			}
		}

	}