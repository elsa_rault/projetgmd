package drugbank;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import Utils.Disease;
import Utils.Drug;

/** Class for searching into index drugbank
 * 
 * @author Elsa and Clementine
 *
 */
public class XMLSearch {

	private ArrayList<Drug> drugs;
	private ArrayList<Drug> parentsDrugs;
	private static ArrayList<String> name;
	private static ArrayList<String> indications ;
	private static ArrayList<String> toxicity ;


	/** Test
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{

		//Searching	
		System.out.println("search...  ");
		ArrayList<Disease> diseases= XMLSearch("throat*");
		for (int i=0; i<diseases.size(); i++){
			System.out.println("*****************************");
			for (int j=0; j<diseases.get(i).getDrugs().size(); j++){
				System.out.println(j+": "+diseases.get(i).getDrugs().get(j));
				
			}
			System.out.println("*****************************");
			for (int j=0; j<diseases.get(i).getParentsDrugs().size(); j++){
				System.out.println(j+": "+diseases.get(i).getParentsDrugs().get(j));
			}
		}
	}

	/** This method allows to recover a list of Disease element with the name of the disease
	 * 
	 * @param disease
	 * @return list of disease name
	 */
	public static ArrayList<Disease> XMLSearch(String disease) throws ParseException{
		
		//initialize all parameters that are mandatory to create a disease element
		ArrayList<Disease> d= new ArrayList<Disease>();
		ArrayList<Drug> drugIndicatedForDisease= drugIndicatedForDisease(disease);
		ArrayList<Drug> drugCausedDisease= drugCausedDisease(disease);
		ArrayList<String> source= new ArrayList<String>();
		source.add("DrugBank");
		
		//recover all drugs that cure this disease
		ArrayList<String> allName1 = getAllNameIndications(disease,drugIndicatedForDisease);
		
		//recover all drugs that cause this disease
		ArrayList<String> allName2 = getAllNamedrugCausedDisease(disease,drugCausedDisease);
		for (int i=0; i<allName2.size(); i++){
			if(!allName1.contains(allName2.get(i))){
				allName1.add(allName2.get(i));
			}
		}
		
		for (int k=0; k<allName1.size(); k++){
			//create all drug element
			ArrayList<Drug> drugInd= drugIndicatedForDisease(allName1.get(k));
			ArrayList<Drug> drugCaused= drugCausedDisease(allName1.get(k));
			
			//create disease
			Disease dis=  new Disease(allName1.get(k).toUpperCase(), null,null,drugIndicatedForDisease,drugCausedDisease,source,0);
			d.add(dis);
		}
		
		return d;
	}

	/** This method is used to search into drugbank index
	 * 
	 * @param line: query 
	 * @param field
	 */
	public static void Search(String line,String field) throws ParseException {
		name=new ArrayList<String>();
		indications =new ArrayList<String>();
		toxicity =new ArrayList<String>();
		String index = XMLIndex.INDEX_PATH;
		Directory dir;
		IndexReader reader;
		try {
			dir=FSDirectory.open(Paths.get(index));
			reader = DirectoryReader.open(dir);
			IndexSearcher searcher = new IndexSearcher(reader);
			Analyzer analyzer = new StandardAnalyzer();
			BufferedReader in = null;

			int hitsPerPage = searcher.getIndexReader().maxDoc();
			in = new BufferedReader(new InputStreamReader(System.in, "UTF-8"));



			String terms[]=line.split(" ");
			BooleanQuery query= new BooleanQuery();
			for (int i=0; i<terms.length;i++){
				WildcardQuery searchQuery= new WildcardQuery(new Term(field,terms[i]));
				query.add(new BooleanClause(searchQuery, Occur.MUST));
			}
			//System.out.println("query: " +query);

			QueryParser parser = new QueryParser(field,analyzer);
			parser.setDefaultOperator(QueryParser.Operator.AND);

			Query query1 = parser.parse(line);

			TopDocs hits = searcher.search(query1, 100);
			//System.out.println("Found: " + hits.totalHits);

			for(ScoreDoc scoreDoc : hits.scoreDocs){
				Document doc = searcher.doc(scoreDoc.doc);
				name.add(doc.get("Name"));
				//System.out.println(name);
				indications.add(doc.get("Indication"));
				//System.out.println(indications);
				toxicity.add(doc.get("Toxicity"));
				//System.out.println(toxicity);
			}
			reader.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	} 

	/** This method handles wildcards and recover all diseases names  for indications of a drug
	 * 
	 * @param disease: name of the disease
	 * @param drugList: list of drug
	 * @return
	 */
	public static ArrayList<String> getAllNameIndications(String disease,ArrayList<Drug> drugList){
		disease=disease.replaceAll("\\*",".*");
		disease=disease.replaceAll("\\?",".?");
		ArrayList<String> name= new ArrayList<String>();
		if(drugList!=null){
			for (int i=0; i<drugList.size(); i++){
				ArrayList<String> nameDisease= drugList.get(i).getIndications();
				for (int j=0; j<nameDisease.size(); j++){
					String changes= nameDisease.get(j);
					changes=changes.replaceAll(",","");
					changes=changes.replaceAll(";","");
					changes=changes.replaceAll("\\.","");
					changes=changes.replaceAll("\\(","");
					changes=changes.replaceAll("\\)","");
					String fields[] = changes.split(" ");
					for (int h=0; h<fields.length; h++){

						if (fields[h].matches(disease) && !name.contains(fields[h])){

							name.add(fields[h]);
						}
					}
				}
			}
		}
		
		return name;
	}
	
	/** This method handles wildcards and recovers all diseases names for side effects of a drug 
	 * 
	 * @param disease: name of the disease
	 * @param drugList: list of drug
	 * @return
	 */
	public static ArrayList<String> getAllNamedrugCausedDisease(String disease,ArrayList<Drug> drugList){
		disease=disease.replaceAll("\\*",".*");
		disease=disease.replaceAll("\\?",".?");
		ArrayList<String> name= new ArrayList<String>();
		if (drugList!=null){
			for (int i=0; i<drugList.size(); i++){
				ArrayList<String> nameDisease= drugList.get(i).getSide_effect();
				for (int j=0; j<nameDisease.size(); j++){
					String changes= nameDisease.get(j);
					changes=changes.replaceAll(",","");
					changes=changes.replaceAll(";","");
					changes=changes.replaceAll("\\.","");
					changes=changes.replaceAll("\\(","");
					changes=changes.replaceAll("\\)","");
					String fields[] = changes.split(" ");
					for (int h=0; h<fields.length; h++){

						if (fields[h].matches(disease) && !name.contains(fields[h])){

							name.add(fields[h]);
						}
					}
				}
			}
		}
		

		return name;
	}

	/** This method recovers all informations for a drug and create a drug element
	 * 
	 * @param drug: name of the drug
	 * @return
	 * @throws ParseException
	 */
	public Drug drug(String drug) throws ParseException{
		Drug drugList= null;
		ArrayList<String> nameDrug= new ArrayList<String>();
		Search(drug,"Name");
		nameDrug= getName();
		ArrayList<String> sources = new ArrayList<String>();
		sources.add("DrugBank");
		if (!nameDrug.isEmpty()){
			String name=nameDrug.get(0);
			
			Drug d= new Drug(name,getIndications(), getToxicity(),sources,1);
			drugList=d;			
		}
		return drugList;
	}
	
	/** This method recovers all drugs that cause this disease
	 * 
	 * @param disease: the name of the disease
	 * @return  list of drug that cures this disease
	 */
	public static ArrayList<Drug> drugCausedDisease(String disease) throws ParseException{
		ArrayList<Drug> drugList= null;
		Search(disease,"Toxicity");
		ArrayList<String> nameDrugCausedDisease= new ArrayList<String>();
		nameDrugCausedDisease=getName();
		ArrayList<String> sources = new ArrayList<String>();
		sources.add("DrugBank");
		if (!nameDrugCausedDisease.isEmpty()){
			drugList=new ArrayList<Drug>();
			for (int i=0; i<nameDrugCausedDisease.size();i++){
				Search(nameDrugCausedDisease.get(i),"Name");
				Drug d= new Drug(nameDrugCausedDisease.get(i),getIndications(), getToxicity(),sources,1);
				drugList.add(d);
			}

			return drugList;
		}
		return drugList;
	}

	/** This method recovers all drugs that cause this disease
	 * 
	 * @param disease: the name of the disease
	 * @return  list of drug that causes this disease
	 */
	public static ArrayList<Drug> drugIndicatedForDisease(String disease) throws ParseException{
		ArrayList<Drug> drugList= new ArrayList<Drug>();
		Search(disease,"Indication");
		ArrayList<String> nameDrugIndicatedForDisease= new ArrayList<String>();
		nameDrugIndicatedForDisease=getName();
		ArrayList<String> sources = new ArrayList<String>();
		sources.add("DrugBank");
		if (!nameDrugIndicatedForDisease.isEmpty()){
			for (int i=0; i<nameDrugIndicatedForDisease.size();i++){
				Search(nameDrugIndicatedForDisease.get(i),"Name");
				Drug d= new Drug(nameDrugIndicatedForDisease.get(i),getIndications(), getToxicity(),sources,1);
				drugList.add(d);
			}
			return drugList;
		}
		drugList=null;
		return drugList;
	}

	public ArrayList<Drug> getDrugs() {
		return drugs;
	}

	public void setDrugs(ArrayList<Drug> drugs) {
		this.drugs = drugs;
	}

	public ArrayList<Drug> getParentsDrugs() {
		return parentsDrugs;
	}

	public void setParentsDrugs(ArrayList<Drug> parentsDrugs) {
		this.parentsDrugs = parentsDrugs;
	}

	public static ArrayList<String> getName() {
		return name;
	}

	public static void setName(ArrayList<String> name) {
		XMLSearch.name = name;
	}

	public static ArrayList<String> getIndications() {
		return indications;
	}

	public static void setIndications(ArrayList<String> indications) {
		XMLSearch.indications = indications;
	}

	public static ArrayList<String> getToxicity() {
		return toxicity;
	}

	public static void setToxicity(ArrayList<String> toxicity) {
		XMLSearch.toxicity = toxicity;
	}
}
